%%  Run where instructed in AnalysisInstructionsMPC_... file

%% Figure 1 - Actions
hemiCode = 1+cellfun(@(x) strcmp(x,'R'),MPCNeuronStruct_Final.hemisphere(allTurnsNODSIncRandomPopStats.validTest == true))';
hemiPrefCode = 1+(allTurnsNODSIncRandomPopStats.meanRFR(allTurnsNODSIncRandomPopStats.validTest)>...
    allTurnsNODSIncRandomPopStats.meanLFR(allTurnsNODSIncRandomPopStats.validTest));

ipsiPrefer = sum(hemiPrefCode == hemiCode & allTurnsNODSIncRandomPopStats.ranksumP(allTurnsNODSIncRandomPopStats.validTest)<0.05);
contraPrefer = sum(hemiPrefCode ~= hemiCode & allTurnsNODSIncRandomPopStats.ranksumP(allTurnsNODSIncRandomPopStats.validTest)<0.05);
% a results text stat
binomP = myBinomTest(ipsiPrefer,ipsiPrefer+contraPrefer,0.5);

% with chi square Goodness of Fit
hemiCodeJustSig = hemiCode(allTurnsNODSIncRandomPopStats.ranksumP(allTurnsNODSIncRandomPopStats.validTest)<0.05);
hemiPrefCodeJustSig = hemiPrefCode(allTurnsNODSIncRandomPopStats.ranksumP(allTurnsNODSIncRandomPopStats.validTest)<0.05);
[H,P,STATS] = chi2gof((hemiPrefCodeJustSig == hemiCodeJustSig) +1, 'nbins', 2, 'expected', [length(hemiCodeJustSig)/2, length(hemiCodeJustSig)/2]);
% H =     0
% P =    0.9443
% STATS = 
%     chi2stat: 0.0049`
%           df: 1
%        edges: [1.0000 1.5000 2.0000]
%            O: [103 102]
%            E: [102.5000 102.5000]
%            0.94
%binom p = 1, all neurons, all turns pooled with full samples

%% Tracking Example - 1B
Figure1B_TrackingExample;

%% Example neurons - 1DE
Figure1DEScript;

% chance value - 95th percentile of the random dist.
aucRandomPopSorted = sort(oneSidedAUCrandomPop);
chanceLevel95 = aucRandomPopSorted(round(sum(~isnan(oneSidedAUCrandomPop))*0.95)); % 0.5674

%% Compare stats of 1st turn and sample size matched all. - Figure 1F, results stats
nSig1 = sum(spaceTurn1Stats.ranksumP(spaceTurn1Stats.validTest==1)<0.05);
% 168
nSigAll = sum(allTurnsStats.ranksumP(allTurnsStats.validTest==1)<0.05);
% 154
nTested1 = sum(spaceTurn1Stats.validTest);
% 296
nTestedAll = sum(allTurnsStats.validTest);
% 303

validEventCounts = spaceTurn1Stats.nEventsUsed(:,spaceTurn1Stats.validTest);
mean(validEventCounts(:));
% 31.9713
std(validEventCounts(:));
% 14.1556

validEventCounts = allTurnsStats.nEventsUsed(:,allTurnsStats.validTest);
mean(validEventCounts(:));
% 31.6287
std(validEventCounts(:));
% 10.7084

validEventCounts = allTurnsNODSIncRandomPopStats.nEventsUsed(:,allTurnsNODSIncRandomPopStats.validTest);
mean(validEventCounts(:));
% 130.5627
std(validEventCounts(:));
% 51.9661

% This is figure 1F
figure;
bar([nSig1/nTested1,nSigAll/nTestedAll]); 
[hFisher,pFisher] = fishertest([nSig1,nSigAll;nTested1-nSig1,nTestedAll-nSigAll]);
% pFisher = 0.1636

testTotals = [ones(nTested1,1);zeros(nTestedAll,1)];
testValues = [zeros(nSig1,1); ones(nTested1-nSig1,1); zeros(nSigAll,1); ones(nTestedAll-nSigAll,1)];
[tbl,chi2,p] = crosstab(testTotals,testValues);
% tbl =
%    154   149
%    168   128
% chi2 =  2.1192
% p =    0.1455
odds_ratio =154*128/(149*168);
% odds_ratio =
%     0.7875

%% Aggregating individual turn results for all neurons 
% if not run as part of full analysis script, may need to do this:
% loadAndAggregateTurnResults;

%% Figure 1GHI space L/R differences and results text stats
stepSize = 0.05;
oneSidedAUCMeanActionAll = nanmean([oneSidedAUCSpace1;oneSidedAUCSpace2;oneSidedAUCSpace3;oneSidedAUCSpace4;oneSidedAUCSpace5;oneSidedAUCSpace6;oneSidedAUCSpace7],1);

% sum(oneSidedAUCMeanActionAll > 2/3 & ~isnan(oneSidedAUCMeanActionAll))./sum(~isnan(oneSidedAUCMeanActionAll))
%     0.5418
% sum(oneSidedAUCMeanActionAll > 0.9 ~isnan(oneSidedAUCMeanActionAll))./sum(~isnan(oneSidedAUCMeanActionAll))
%     0.1438

% sum(oneSidedAUCAllNoDS > 2/3 & ~isnan(oneSidedAUCAllNoDS))./sum(~isnan(oneSidedAUCAllNoDS))
%     0.4521 
% sum(oneSidedAUCAllNoDS > 0.9 & ~isnan(oneSidedAUCAllNoDS))./sum(~isnan(oneSidedAUCAllNoDS))
%     0.1089


% [~, pKSOneVsAll] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCMeanActionAll);
% %   p < 0.001
% [~, pKSOneVsShuffled] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCrandomPop);
% %   p < 0.0001
% [~, pKSAllVsShuffled] = kstest2(oneSidedAUCrandomPop,oneSidedAUCMeanActionAll);
% %   p < 0.0001
% [pKW,anovaKW] = kruskalwallis(allData,allDataLabels);
% Source       SS       df       MS      Chi-sq   Prob>Chi-sq
% -----------------------------------------------------------
% Groups     828101.1     6   138016.8   10.24       0.115   
% Error    78770888.9   978    80542.8                       
% Total    79598990     984                                  

% [pKW,anovaKW, STATS] = kruskalwallis(allData,allDataLabels);


% Figure 1G
figure;
hold on;
% cdfplot(oneSidedAUCrandomPop);
% cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCSpace1);
cdfplot(oneSidedAUCSpace2);
cdfplot(oneSidedAUCSpace3);
cdfplot(oneSidedAUCSpace4);
cdfplot(oneSidedAUCSpace5);
cdfplot(oneSidedAUCSpace6);
cdfplot(oneSidedAUCSpace7);

% Figure 1H
figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS); % all turns, lumped together, treated as one.
cdfplot(oneSidedAUCMeanActionAll); % avg of results of ind. turns

% Figure 1I
figure;
histogram(oneSidedAUCAllNoDS,0.5:stepSize:1,'Normalization','probability');
% figure;
% histogram(oneSidedAUCAllNoDS,0.5:0.1:1,'Normalization','probability');
% %Histogram values of auc(CP) values with line graph style
% figure;
% hold on;
% % [n1,wout1] = hist(oneSidedAUCrandomPop,0.5:stepSize:1);
% % plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCAllNoDS,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCSpace1,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCSpace2,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCSpace3,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCSpace4,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCSpace5,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCSpace6,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));
% [n1,wout1] = hist(oneSidedAUCSpace7,0.5:stepSize:1);
% plot(wout1,n1/sum(n1));

%% Not currently included - relationship between mean firing rate and discriminability
trackMeanFR = MPCNeuronStruct_Final.trackMeanFR;
figure;
plot(oneSidedAUCAllNoDS,trackMeanFR,'.')
ylabel('AUC of all turns no ds')
xlabel('meanFR on track')

meanSideFRs = [allTurnsNODSIncRandomPopStats.meanLFR,allTurnsNODSIncRandomPopStats.meanRFR];
figure;
plot(oneSidedAUCAllNoDS,max(meanSideFRs,[],2),'.');
hold on;
plot(oneSidedAUCAllNoDS(hemiPrefCode==hemiCode),max(meanSideFRs(hemiPrefCode==hemiCode,:),[],2),'.');
legend('contra prefer','ipsa prefer')
ylabel('meanFR of preferred side')
xlabel('AUC of all turns no ds')

figure;
plot(oneSidedAUCAllNoDS,min(meanSideFRs,[],2),'.');
hold on;
plot(oneSidedAUCAllNoDS(hemiPrefCode==hemiCode),min(meanSideFRs(hemiPrefCode==hemiCode,:),[],2),'.');
legend('contra prefer','ipsa prefer')
ylabel('meanFR of nonpreferred side side')
xlabel('AUC of all turns no ds')
axis([0.5,1,0,70])

figure;
plot(oneSidedAUCAllNoDS,trackMeanFR,'.');
hold on;
plot(oneSidedAUCAllNoDS(hemiPrefCode==hemiCode),trackMeanFR(hemiPrefCode==hemiCode),'.');
legend('contra prefer','ipsa prefer')
ylabel('meanFR of nonpreferred side side')
xlabel('AUC of all turns no ds')
axis([0.5,1,0,70])

figure;
plot(oneSidedAUCAllNoDS(hemiPrefCode==hemiCode),max(meanSideFRs(hemiPrefCode==hemiCode,:),[],2),'.');
ylabel('meanFR of preferred side - ipsa')
xlabel('AUC of all turns no ds')
axis([0.5,1,0,70])

figure;
plot(oneSidedAUCAllNoDS(hemiPrefCode~=hemiCode),max(meanSideFRs(hemiPrefCode~=hemiCode,:),[],2),'.');
ylabel('meanFR of preferred side - contra')
xlabel('AUC of all turns no ds')
axis([0.5,1,0,70])

% minimum FRs don't correlate w/ neuron's action discriminability
corrcoef(min(meanSideFRs,[],2),oneSidedAUCAllNoDS,'rows','Complete')
%     1.0000    0.0210
%     0.0210    1.0000

% max, mean FRs do correlate w/ neuron's action discriminability
corrcoef(max(meanSideFRs,[],2),oneSidedAUCAllNoDS,'rows','Complete')
%     1.0000    0.4350
%     0.4350    1.0000
corrcoef(MPCNeuronStruct_Final.trackMeanFR,oneSidedAUCAllNoDS,'rows','Complete')
%     1.0000    0.2055
%     0.2055    1.0000

%ipsa prefer
corrcoef(MPCNeuronStruct_Final.trackMeanFR(hemiPrefCode==hemiCode),oneSidedAUCAllNoDS(hemiPrefCode==hemiCode),'rows','Complete')
%     1.0000    0.0624
%     0.0624    1.0000
%contra prefer
corrcoef(MPCNeuronStruct_Final.trackMeanFR(hemiPrefCode~=hemiCode),oneSidedAUCAllNoDS(hemiPrefCode~=hemiCode),'rows','Complete')
%     1.0000    0.0304
%     0.0304    1.0000
