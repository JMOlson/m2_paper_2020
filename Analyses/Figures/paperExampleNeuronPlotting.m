
%% Plot sample neurons - load and order
load('figureColormap.mat');
mapToUse = hotMapClipped;
load([projectFolder,'\Data\AnalysisResults_CurrBioRev\TurnAlignedSpaceRasters.mat']);

sortCol = 1;
[sorted, origIndices] = sortrows(oneSidedAUCCatAllConds,sortCol,'descend');
%%
for i = 1:nNeurons
    if ~isnan(sorted(i,sortCol))
        iNeu = origIndices(i)
        
%         [twoDRMaps] = twoDPathsRateMapper(MPCRecStruct_Final,MPCNeuronStruct_Final,1,1,1,iNeu);
%         twoDRMapsFiltered = filter2DMatrices(twoDRMaps,0);
%         nGoodPoints = numel(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))));
%         goodPointsOrdered = sort(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))),'descend');
%         cMax = round(goodPointsOrdered(round(0.01*nGoodPoints)));
%         for iPath = 1:10
%             figure(iPath)
%             sc(twoDRMapsFiltered(:,:,iPath),[0,cMax],mapToUse,'w',isnan(twoDRMapsFiltered(:,:,iPath)));
%         end
        plotResults = perieventPlotter( 'action', 'sequence', '12345', iNeu, TurnRasters, TurnParameters, ...
            MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10, 'std');
        disp(oneSidedAUCCatAllConds(iNeu,:))
        
        disp('Color Axis Max is: ');
        disp(cMax);
        pause;
    end
end