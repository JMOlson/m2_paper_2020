%% Figure 3 - Spatial Context & Choice
%% setup
% should be run as part of analysis instructions - if not, may need some
% other things.

% load all of the sequence***Stats and the all**Stats files, if
% didn't just run CPAnalyses script
% also Aggregating individual turn results for all neurons 
% loadCPAnalyses;

%% Figure 3B - Looking at correlations between encoded variables
sortedAUCs = sortrows(oneSidedAUCCatAllConds,-1);
corr(oneSidedAUCCatAllConds,'rows','pairwise')
%     1.0000    0.4025    0.4510    0.3831    0.4807    0.2841
%     0.4025    1.0000    0.4359    0.2135    0.5058    0.3297
%     0.4510    0.4359    1.0000    0.4046    0.7022    0.2158
%     0.3831    0.2135    0.4046    1.0000    0.5241    0.2278
%     0.4807    0.5058    0.7022    0.5241    1.0000    0.2791
%     0.2841    0.3297    0.2158    0.2278    0.2791    1.0000

%% Something looking at significant action decoding and what other values are
% a = [sum(oneSidedAUCCatAllConds(:,1)>2/3 & oneSidedAUCCatAllConds(:,2)>2/3),...
% sum(oneSidedAUCCatAllConds(:,1)>2/3 & oneSidedAUCCatAllConds(:,3)>2/3),...
% sum(oneSidedAUCCatAllConds(:,1)>2/3 & oneSidedAUCCatAllConds(:,4)>2/3),...
% sum(oneSidedAUCCatAllConds(:,1)>2/3 & oneSidedAUCCatAllConds(:,5)>2/3)];
% b =  [sum(oneSidedAUCCatAllConds(:,1)<=2/3 & oneSidedAUCCatAllConds(:,2)>2/3),...
% sum(oneSidedAUCCatAllConds(:,1)<=2/3 & oneSidedAUCCatAllConds(:,3)>2/3),...
% sum(oneSidedAUCCatAllConds(:,1)<=2/3 & oneSidedAUCCatAllConds(:,4)>2/3),...
% sum(oneSidedAUCCatAllConds(:,1)<=2/3 & oneSidedAUCCatAllConds(:,5)>2/3)];
% [a/125;b/(331-125)]

%% sequence and choice L/R differences - not a figure
figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCSequence1);
cdfplot(oneSidedAUCSequence2);
cdfplot(oneSidedAUCSequence3);
cdfplot(oneSidedAUCChoice(:,1));
cdfplot(oneSidedAUCChoice(:,2));
legend('random','allturns','sequence1','sequence2','sequence3','choice','forced')

%% Figure 4b - choice vs forced effect on action discrimination
figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCChoice(:,1));
cdfplot(oneSidedAUCChoice(:,2));
legend('random','allturns','choice','forced');


 [hKSChoice, pKSChoice,d] = kstest2(oneSidedAUCChoice(:,1),oneSidedAUCChoice(:,2));
 [hKSChoice, pKSChoiceVsAll,d] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCChoice(:,1));
 [hKSChoice, pKSForcedVsAll,d] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCChoice(:,2));
% pKSChoice =  0.7962
% d =  0.0562
% pKSChoiceVsAll =  0.5319
% d =    0.0651
% pKSForcedVsAll = 0.2704
% d = 0.0865 
% Not different from each other or the all turns

%% Figure 3a and 4c  - choiced context as compared to action
figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCMeanActionAll);
% Comment these four lines out for Figure 4c, uncomment for 3a
% cdfplot(oneSidedAUCSequenceEffectAll);
% cdfplot(oneSidedAUCSpaceEffectAll);
% cdfplot(oneSidedAUCOrientationEffectAll);
% cdfplot(oneSidedAUCPathEffectAll);
% Comment this line out for Figure 3a, uncomment for 4b
cdfplot(oneSidedAUCChoiceEffectAll);
title('mean')
% legend({'random','l/r all','progress','location','orientation','route','choice'})

context66 = sum(oneSidedAUCCatAllConds>2/3)./sum(~isnan(oneSidedAUCCatAllConds));
context90 = sum(oneSidedAUCCatAllConds>0.9)./sum(~isnan(oneSidedAUCCatAllConds));
figure;
bar([context66([1,2,3,6,5,4]);context90([1,2,3,6,5,4])]);

% % Statistics on these graphs?
% for a = 1:5
% [h66(a),p66(a)] = fishertest([sum(oneSidedAUCCatAllConds(:,1)>2/3),sum(oneSidedAUCCatAllConds(:,a+1)>2/3);...
% sum(~isnan(oneSidedAUCCatAllConds(:,1)))-sum(oneSidedAUCCatAllConds(:,1)>2/3),sum(~isnan(oneSidedAUCCatAllConds(:,a+1)))-sum(oneSidedAUCCatAllConds(:,a+1)>2/3)]);
% [h90(a),p90(a)] = fishertest([sum(oneSidedAUCCatAllConds(:,1)>.9),sum(oneSidedAUCCatAllConds(:,a+1)>.9);...
% sum(~isnan(oneSidedAUCCatAllConds(:,1)))-sum(oneSidedAUCCatAllConds(:,1)>.9),sum(~isnan(oneSidedAUCCatAllConds(:,a+1)))-sum(oneSidedAUCCatAllConds(:,a+1)>.9)]);
% end

% figure 3a panels 3 and 4
figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCMinActionAll);
cdfplot(oneSidedAUCMinSequenceEffectAll);
cdfplot(oneSidedAUCMinSpaceEffectAll);
cdfplot(oneSidedAUCMinOrientationEffectAll);
cdfplot(oneSidedAUCMinPathEffectAll);
cdfplot(oneSidedAUCMinChoiceEffectAll);
% h = cdfplot(oSAUCNewRandomMinAll);
% set(h,'LineWidth',3);
title('min')
legend({'random','l/r all','progress','location','orientation','route','choice','r'})

figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCMaxActionAll);
cdfplot(oneSidedAUCMaxSequenceEffectAll);
cdfplot(oneSidedAUCMaxSpaceEffectAll);
cdfplot(oneSidedAUCMaxOrientationEffectAll);
cdfplot(oneSidedAUCMaxPathEffectAll);
cdfplot(oneSidedAUCMaxChoiceEffectAll);
% h = cdfplot(oSAUCNewRandomMaxAll);
% set(h,'LineWidth',3);
title('max')
legend({'random','l/r all','progress','location','orientation','route','choice','r'})


%% context discriminability - all pairs - not a figure, but maybe should be supplementary
figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCSequenceEffect(1,:,1));
cdfplot(oneSidedAUCSequenceEffect(1,:,2));
cdfplot(oneSidedAUCSequenceEffect(1,:,3));
cdfplot(oneSidedAUCSequenceEffect(2,:,1));
cdfplot(oneSidedAUCSequenceEffect(2,:,2));
cdfplot(oneSidedAUCSequenceEffect(2,:,3));

figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCSpaceEffect23(1,:));
cdfplot(oneSidedAUCSpaceEffect23(2,:));

figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCSpaceEffect4567(1,:,1));
cdfplot(oneSidedAUCSpaceEffect4567(1,:,2));
cdfplot(oneSidedAUCSpaceEffect4567(1,:,3));
cdfplot(oneSidedAUCSpaceEffect4567(1,:,4));
cdfplot(oneSidedAUCSpaceEffect4567(1,:,5));
cdfplot(oneSidedAUCSpaceEffect4567(1,:,6));
cdfplot(oneSidedAUCSpaceEffect4567(2,:,1));
cdfplot(oneSidedAUCSpaceEffect4567(2,:,2));
cdfplot(oneSidedAUCSpaceEffect4567(2,:,3));
cdfplot(oneSidedAUCSpaceEffect4567(2,:,4));
cdfplot(oneSidedAUCSpaceEffect4567(2,:,5));
cdfplot(oneSidedAUCSpaceEffect4567(2,:,6));

figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCChoiceEffect(1,:));
cdfplot(oneSidedAUCChoiceEffect(2,:));

figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCPathEffect(1,:,1));
cdfplot(oneSidedAUCPathEffect(1,:,2));
cdfplot(oneSidedAUCPathEffect(1,:,3));
cdfplot(oneSidedAUCPathEffect(1,:,4));
cdfplot(oneSidedAUCPathEffect(1,:,5));
cdfplot(oneSidedAUCPathEffect(1,:,6));
cdfplot(oneSidedAUCPathEffect(2,:,1));
cdfplot(oneSidedAUCPathEffect(2,:,2));
cdfplot(oneSidedAUCPathEffect(2,:,3));
cdfplot(oneSidedAUCPathEffect(2,:,4));
cdfplot(oneSidedAUCPathEffect(2,:,5));
cdfplot(oneSidedAUCPathEffect(2,:,6));

figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(oneSidedAUCOrientationEffect(1,:,1));
cdfplot(oneSidedAUCOrientationEffect(1,:,2));
cdfplot(oneSidedAUCOrientationEffect(1,:,3));
cdfplot(oneSidedAUCOrientationEffect(1,:,4));
cdfplot(oneSidedAUCOrientationEffect(1,:,5));
cdfplot(oneSidedAUCOrientationEffect(1,:,6));
cdfplot(oneSidedAUCOrientationEffect(2,:,1));
cdfplot(oneSidedAUCOrientationEffect(2,:,2));
cdfplot(oneSidedAUCOrientationEffect(2,:,3));
cdfplot(oneSidedAUCOrientationEffect(2,:,4));
cdfplot(oneSidedAUCOrientationEffect(2,:,5));
cdfplot(oneSidedAUCOrientationEffect(2,:,6));

%%
figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS);
cdfplot(nanmean(nanmean(oneSidedAUCSequenceEffect,3),1));
cdfplot(nanmean(oneSidedAUCSpaceEffect23,1));
cdfplot(nanmean(nanmean(oneSidedAUCSpaceEffect4567,3),1));
cdfplot(nanmean(oneSidedAUCChoiceEffect,1));
cdfplot(nanmean(nanmean(oneSidedAUCOrientationEffect,3),1));
cdfplot(nanmean(nanmean(oneSidedAUCPathEffect,3),1));


%% fig 3 text results
[~, pKS_RProg, D] = kstest2(oneSidedAUCrandomPop,oneSidedAUCSequenceEffectAll,'tail','larger');
% pKS_RProg =   7.4609e-73
% D =    0.7327
[~, pKS_RLoc, D] = kstest2(oneSidedAUCrandomPop,oneSidedAUCSpaceEffectAll,'tail','larger');
% pKS_RLoc =   7.5246e-68
% D =    0.7217
[~, pKS_RHD, D] = kstest2(oneSidedAUCrandomPop,oneSidedAUCOrientationEffectAll,'tail','larger');
% pKS_RHD =   6.2601e-83
% D =    0.7822
[~, pKS_RRoute, D] = kstest2(oneSidedAUCrandomPop,oneSidedAUCPathEffectAll,'tail','larger');
% pKS_RRoute =   5.3777e-54
% D =   0.6522
[~, pKS_LRProg, D] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCSequenceEffectAll,'tail','smaller');
% pKS_LRProg =   6.5570e-07
% D =    0.2145
[~, pKS_LRLoc, D] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCSpaceEffectAll,'tail','smaller');
% pKS_LRLoc =  5.3435e-04
% D =  0.1593
[~, pKS_LRHD, D] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCOrientationEffectAll,'tail','smaller');
% pKS_LRHD =  2.1033e-09
% D =  0.2541
[~, pKS_LRRoute, D] = kstest2(oneSidedAUCAllNoDS,oneSidedAUCPathEffectAll,'tail','smaller');
% pKS_LRRoute =  2.8788e-10
% D =    0.2760



