%% Olson ... Nitz MPC Paper
% Figure Supplemental - Example Neurons
% 90+
% iNeu = 156; % r plan -  example DN68
% iNeu = 10; % L plan -  example NS13
% iNeu = 138; % r action -  example DN68
% iNeu = 20; % l action -  example NS13

% iNeu = 77; % l action - left example DN66
% iNeu = 46; % r action - right example  NS14
% iNeu = 28; % l plan - middle example NS14

% ~2/3
% iNeu = 76; % ?? -  example DN66
% iNeu = 233; % r plan -  example DN738

load('D:\NitzLab\NitzLabProjects\MPCPaperProject\Data\AnalysisResults_CurrBioRev\TurnAlignedSpaceRasters.mat');
load('figureColormap.mat');
mapToUse = hotMapClipped;

% Neurons used: 46, 77, 28
% neuronList90 = [257, 143, 152, 20, 279, 32, 138, 2, 303, 121, 10, 133, 207, 55];
neuronListPlan90 = [193, 137, 211, 156, 90, 149, 167];
neuronList66 = [];
neuronListPlan66 = [];
neuronListFinal = [156, 10, 138, 20, 76, 233]; % 
% neuronListOther
% 19 - conjunction space x progress

neuronList = neuronListFinal; % origIndices;
relCol = 1;

%   snippet to step through and look at all neurons 1 by 1 for example
for i = 1:numel(neuronList)
    iNeu = neuronList(i);
    if ~isnan(oneSidedAUCCatAllConds(iNeu,relCol))
        fprintf('Rat %i - Rec %i - totNeuron %i \n',MPCNeuronStruct_Final.rat(iNeu),...
            MPCNeuronStruct_Final.rec(iNeu),iNeu);
        [twoDRMaps] = twoDPathsRateMapper(MPCRecStruct_Final,MPCNeuronStruct_Final,1,1,1,iNeu);
        twoDRMapsFiltered = filter2DMatrices(twoDRMaps,0);
        
        % Scale to value at 10% percentile of FRs sampled as max (otherwise max washes
        % out differences
        nGoodPoints = numel(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))));
        goodPointsOrdered = sort(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))),'descend');
        cMax = round(goodPointsOrdered(round(0.01*nGoodPoints)));
        
        % Plot 2D paths - each separately
        for iPath = 1:10
            figure;
            sc(twoDRMapsFiltered(:,:,iPath),[0,cMax],mapToUse,'w',isnan(twoDRMapsFiltered(:,:,iPath)));
        end
        
        % Plot perievent 2D ratemaps around turns
        plotResults = perieventPlotter( 'action', 'sequence', '12345', iNeu, TurnRasters, TurnParameters, ...
            MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10, 'std');
        % possible plot types
        %       'indData' - all individual traversals
        %       'meanOnly' - just avg of each group
        %       'sem' - means plus std err of the means
        %       'std' - means plus std deviation of the samples
        %       '95CI' - means plus 95% conf interval of means (bootstrapped)
        %       '99CI' -  - means plus 99% conf interval of means (bootstrapped)
        
        % print out choice probability value
        disp(oneSidedAUCAllNoDS(iNeu));
        figure;
        bar([oneSidedAUCCatAllMinConds(iNeu,:);...
            oneSidedAUCCatAllConds(iNeu,:);...
            oneSidedAUCCatAllMaxConds(iNeu,:)]);
        axis([0.5,4.5,0.5,1])
        legend({'L/R Action', 'Location', 'Progress', 'Orientation', 'Route', 'Choice'});

%         figure;
%         % AIC as percent of full model. - plotted so less AIC is up
%         hBar = bar(modelCriteriaResults(iNeu,:)./fullModelGLM_Stats.modelCriteriaResults_mean(modelCriteriaIndex,iNeu));
%         hBar(1).BaseValue = 1;
%         ax = gca;
%         ax.YDir = 'reverse';
%         legend({'L/R Action', 'Location', 'Progress', 'Orientation', 'Route', 'Choice'});

%         figure;
%         % mean abs error as percent of mean 95BS of single predictor models. - plotted so less error is up
%         hBar = bar([fullModelGLM_Stats.modelSigFit_mae_ModelShuffle(iNeu),modelMAEMat(iNeu,:)]./mean(modelMAE_95_Mat(iNeu,:)));
%         hBar(1).BaseValue = 1;
%         axis([0.5,7.5,0.4,1.1])
%         ax = gca;
%         ax.YDir = 'reverse';
%         legend({'full','L/R Action', 'Location', 'Progress', 'Orientation', 'Route', 'Choice'});

        figure;
%         title('each normed to its own 95');
        % mean abs error as percent of mean 95BS of single predictor models. - plotted so less error is up
        hBar = bar([fullModelGLM_Stats.modelSigFit_mae_ModelShuffle(iNeu),modelMAEMat(iNeu,:)]./[fullModelGLM_Stats.modelSigFit_mae_ModelShuffle95(iNeu),modelMAE_95_Mat(iNeu,:)]);
        hBar(1).BaseValue = 1;
        axis([0.5,7.5,0.4,1.1])
        ax = gca;
        ax.YDir = 'reverse';
%         title('each normed to its own 95');


%         figure;
%         bar(modelIsSig(iNeu,:));
%         legend({'L/R Action', 'Location', 'Progress', 'Orientation', 'Route', 'Choice'});
        
        
        disp('Color Axis Max is: ');
        disp(cMax);
        pause;
        close all;
    end
end