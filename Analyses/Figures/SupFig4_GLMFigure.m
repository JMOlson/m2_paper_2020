%% Supplemental 4 - GLM

%% Calculate values - # significant, significant predictors, best predictor for each neuron
clear statsCalc_postGLM
predictorList = {'action'; 'location'; 'progress'; 'orientation'; 'route'; 'choice'};
nModels = length(predictorList);
for iModel = 1:nModels
    thisModel = onePredictorGLMResults(iModel);
    statsCalc_postGLM(iModel) = statsCalc_postglm(thisModel);
end
fullModelGLM_Stats = statsCalc_postglm(fullGLMResults);
% Choose which model criteria to use.
% four different model criteria calculated in matlab GLM. I look at all four. Basically, don't
% qualitatively change results, so it doesn't really matter which you pick, but I thought basic AIC
% assumptions are the best so I use that.
modelCriteria  = 'AIC';
modelCriteriaIndex = find(cellfun(@(x) isequal(x,modelCriteria), statsCalc_postGLM(1).modelCriteria)==true);

%% Significant predictors for any particular neuron
% iCell = 1;
% for iModel = 1:nModels
% fprintf('%s: %f, Sig?: %i\n',modelCriteria,...
%     statsCalc_postGLM(iModel).modelCriteriaResults_mean(modelCriteriaIndex,iCell),...
%     statsCalc_postGLM(iModel).modelSigFit_mae_ModelShuffle95_sig(iCell));
% end

%% A - Plotting tracking examples color coding different predictors.
GLMPredictorPlotting;

%% B/C - # significant for each model:
clear modelCriteriaMat modelIsSig modelCriteriaResults nSigEachModel
modelCriteriaMat = cat(3,statsCalc_postGLM.modelCriteriaResults_mean);
modelMAEMat = squeeze(cat(3,statsCalc_postGLM.modelSigFit_mae_ModelShuffle));
modelMAE_95_Mat = squeeze(cat(3,statsCalc_postGLM.modelSigFit_mae_ModelShuffle95));
modelIsSig = squeeze(cat(3,statsCalc_postGLM.modelSigFit_mae_ModelShuffle95_sigMean));
nGLMNeurons = size(modelCriteriaMat,2);
for iModel = 1:nModels
    modelCriteriaMat(:,~statsCalc_postGLM(iModel).modelSigFit_mae_ModelShuffle95_sigMean,iModel) = NaN;
    nSigEachModel(iModel) = sum(statsCalc_postGLM(iModel).modelSigFit_mae_ModelShuffle95_sigMean);
%     fprintf('%s %i\n', predictorList{iModel},sum(statsCalc_postGLM(iModel).modelSigFit_mae_ModelShuffle95_sig));
end

modelCriteriaResults = cat(3,statsCalc_postGLM.modelCriteriaResults_mean);
modelCriteriaResults = squeeze(modelCriteriaResults(modelCriteriaIndex,:,:));

figure;
bar(nSigEachModel/nGLMNeurons);
xtickangle(gca,-45);
xticklabels(gca,{'action';'location';'progress';'orientation';'route';'choice'});

[bestModelVal, bestModelID] = min(modelCriteriaMat,[],3);
bestModelID(isnan(bestModelVal)) = 0;
figure;
histogram(bestModelID(modelCriteriaIndex,:),-0.5:6.5,'Normalization','probability');
xtickangle(gca,-45);
xticklabels(gca,{'not sig';'action';'location';'progress';'orientation';'route';'choice'});

%  notsig action  loc  prog   HD   route choice
%     15   110    45    45    26    38    24   * basic AIC - using this - doesn't seem to matter,
%     and these assumptions seem most valid.
% 
%     15   110    45    45    25    38    25
% 
%     15   117    35    45    29    27    35
% 
%     15   118    35    46    27    25    37

%% Check good L/R cells vs good space cells & interaction with beh/ which routes were run.
hilo = strcmp(MPCRecStruct_Final.rewardParadigm(MPCNeuronStruct_Final.recCount),'HL');
all4 = strcmp(MPCRecStruct_Final.rewardParadigm(MPCNeuronStruct_Final.recCount),'14');
all8 = strcmp(MPCRecStruct_Final.rewardParadigm(MPCNeuronStruct_Final.recCount),'18');
nNeurons = length(all8);
whichBeh = zeros(nNeurons,1);
whichBeh(hilo) = 1;
whichBeh(all4) = 2;
whichBeh(all8) = 3;

figure;
scatter(bestModelID(1,:),bestModelVal(1,:),'.');

figure;
histogram2(bestModelID(1,:),bestModelVal(1,:));

figure;
histogram2(bestModelID(1,:)',whichBeh);

figure;
subplot(131)
scatter(bestModelID(1,hilo),bestModelVal(1,hilo),'.');
subplot(132)
scatter(bestModelID(1,all4),bestModelVal(1,all4),'.');
subplot(133)
scatter(bestModelID(1,all8),bestModelVal(1,all8),'.');

figure;
subplot(121)
boxplot(bestModelVal(1,bestModelID(1,:)==1),whichBeh(bestModelID(1,:)==1));
subplot(122)
boxplot(bestModelVal(1,bestModelID(1,:)==2),whichBeh(bestModelID(1,:)==2));

clear hilo all4 all8 whichBeh


