%% Figure 3 Example Space and Orientation neurons
% load([projectFolder,'Data\AnalysisResults_CurrBioRev\MPCRecStruct_Final.mat']);
% load([projectFolder,'Data\AnalysisResults_CurrBioRev\MPCNeuronStruct_Final.mat']);

load([projectFolder,'\Analyses\Figures\figureColormap.mat']);
load([projectFolder,'\Data\AnalysisResults_CurrBioRev\TurnAlignedSpaceRasters.mat']);
mapToUse = hotMapClipped;
iNeu = 213; % example 1 fig 3

neuronList = [213];
for i = 1:numel(neuronList)
iNeu = neuronList(i);

[twoDRMaps] = twoDPathsRateMapper(MPCRecStruct_Final,MPCNeuronStruct_Final,1,1,1,iNeu);
twoDRMapsFiltered = filter2DMatrices(twoDRMaps,0);

% Scale to value at 10% percentile of FRs sampled as max (otherwise max washes
% out differences
nGoodPoints = numel(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))));
goodPointsOrdered = sort(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))),'descend');
cMax = round(goodPointsOrdered(round(0.01*nGoodPoints)));

% Plot 2D paths - each separately
for iPath = 1:10
    figure;
    sc(twoDRMapsFiltered(:,:,iPath),[0,cMax],mapToUse,'w',isnan(twoDRMapsFiltered(:,:,iPath)));
end

% Plot perievent 2D ratemaps around turns
if iNeu == 213
% route discriminability for the same location and actions (lefts/rights).
plotResults = perieventPlotter( 'context', 'path', '1', iNeu, TurnRasters, TurnParameters, ...
    MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10, 'meanOnly');

% location discriminability for the same progress and actions (lefts/rights).
plotResults = perieventPlotter( 'context', 'space', '4567', iNeu, TurnRasters, TurnParameters, ...
    MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10, 'meanOnly');

end
% possible plot types
%       'indData' - all individual traversals
%       'meanOnly' - just avg of each group
%       'sem' - means plus std err of the means
%       'std' - means plus std deviation of the samples
%       '95CI' - means plus 95% conf interval of means (bootstrapped)
%       '99CI' -  - means plus 99% conf interval of means (bootstrapped)

% print out choice probability value
oneSidedAUCCatAllConds(iNeu,:)

figure;
bar([oneSidedAUCCatAllMinConds(iNeu,1:5);...
    oneSidedAUCCatAllConds(iNeu,1:5);...
    oneSidedAUCCatAllMaxConds(iNeu,1:5)]);
legend({'L/R Action', 'Location', 'Progress', 'Orientation', 'Route'});
axis([0.5,3.5,0.5,1])

disp('Color Axis Max is: ');
disp(cMax);
    
end