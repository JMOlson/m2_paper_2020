%% Olson ... Nitz MPC Paper -- Supplemental Xa
% Plot Predictors of different types.

load([projectFolder,'Data\AnalysisResults_CurrBioRev\TurnAlignedSpaceRasters.mat']);

colorsToPlot = distinguishable_colors(11,'w');
ParameterList = {'action'; 'location'; 'progress'; 'orientation'; 'route'; 'choice'};
nParameters = length(ParameterList);
iRec = 25;
iNeuron = 77;
nRuns = size(TurnParameters{iNeuron},1);


for iParameter = 1:nParameters
    featureList = unique(TurnParameters{iNeuron}.(ParameterList{iParameter}));
    for iFeature = 1:length(featureList)
        figure;
        ax = gca;
        hold on;
        hasThisFeature = arrayfun(@(x) isequal(x,featureList(iFeature)),TurnParameters{iNeuron}.(ParameterList{iParameter}));
        for iRun = 1:nRuns
            if hasThisFeature(iRun)
                ax.ColorOrderIndex = iFeature;
                xs = TurnPosition3D{iNeuron}{iRun}(:,2);
                ys = TurnPosition3D{iNeuron}{iRun}(:,3);
                badSpots = xs<50 | xs>450 | ys<50 | ys>330; % remove off track from plotting
                badSpots = badSpots | [0; abs(diff(xs)) > 25 | abs(diff(ys)) > 25 ]; % remove jumps from glare from plotting.
                plot(xs(~badSpots),ys(~badSpots),'color',colorsToPlot(iFeature,:));
            end
        end
        axis([50 450 50 350]);
        axis off
    end
end
        
