%% Histogram of neuron pop track mean firing rates: 
figure;
handle = histogram(MPCNeuronStruct_Final.trackMeanFR,0:50);
set(gca,'TickLength',[ 0 0 ]);
set(gca,'XTick',0:10:50);
set(gca,'xLim',[0,50]);
set(gca,'yLim',[0,75]);
set(gca,'yTick',[0:12.5:75]);
set(gca,'yTickLabel',{'','',25,'',50,'',75});
set(gca,'YGrid','on');
set(gca,'GridLineStyle','--'); % for major grid
set(gca,'box','off');
% set(gca,'Layer','top');
set(handle,'FaceColor',[0.4940    0.1840    0.5560]);
set(handle,'FaceAlpha',1);
set(handle,'EdgeColor','k');
set(gcf,'Units','inches');
set(gcf,'papersize',[3 3]);
set(gcf,'paperposition',[0,0,3,3]);

%% log version of neuron pop track mean firing rates: 
figure;
handle = histogram(MPCNeuronStruct_Final.trackMeanFR,logspace(0,log10(50),50));
axH = gca;
set(gca,'xScale','Log');
set(gca,'TickLength',[ 0 0 ]);
set(gca,'yLim',[0,15]);
set(gca,'yTick',[0:5:15]);
set(gca,'yTick',[0:2.5:15]);
set(gca,'yTickLabel',{'','',5,'',10,'',15});
set(gca,'YGrid','on');
set(gca,'GridLineStyle','--');
set(gca,'box','off');
set(handle,'FaceColor',[0.4940    0.1840    0.5560]);
set(handle,'FaceAlpha',1);
set(handle,'EdgeColor','k');
set(gcf,'Units','inches');
set(gcf,'papersize',[3 3]);
set(gcf,'paperposition',[0,0,3,3]);

%%















%%












%%









