%% Analyzing distribution discriminability - Choice Probabiliy - run with different parameter settings - run once, save for later.
% Controlling across behavioral paradigms
rewardParadigm = {MPCRecStruct_Final.rewardParadigm{MPCNeuronStruct_Final.recCount}};
behHLNeurons  = strcmp(rewardParadigm,'HL');
beh8Neurons  = strcmp(rewardParadigm,'14');
beh4Neurons  = strcmp(rewardParadigm,'18');

% allTurnsNODSbehHLNeurons = calcRanksumPerievent( 'action', 'sequence', '12345',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false,behHLNeurons);
% save allTurnsNODSbehHLNeurons allTurnsNODSbehHLNeurons
% allTurnsNODSbeh8Neurons = calcRanksumPerievent( 'action', 'sequence', '12345',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false,beh8Neurons);
% save allTurnsNODSbeh8Neurons allTurnsNODSbeh8Neurons
% allTurnsNODSbeh4Neurons = calcRanksumPerievent( 'action', 'sequence', '12345',MPCNeuronStruct_Final,MPCRecStruct_Final, -20, 10, NaN, false,false,beh4Neurons);
% save allTurnsNODSbeh4Neurons allTurnsNODSbeh4Neurons

%%
load([projectFolder,'Data\AnalysisResults_CurrBioRev\allTurnsNODSbehHLNeurons.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\allTurnsNODSbeh8Neurons.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\allTurnsNODSbeh4Neurons.mat']);

%% Supplemental Figure for Behavioral Effect on action representations
[oneSidedAUCAllNoDSHL, validTestAllNoDSHL] = createAUCMat(allTurnsNODSbehHLNeurons);
oneSidedAUCAllNoDSHL(~validTestAllNoDSHL) = NaN;
[oneSidedAUCAllNoDS8, validTestAllNoDS8] = createAUCMat(allTurnsNODSbeh8Neurons);
oneSidedAUCAllNoDS8(~validTestAllNoDS8) = NaN;
[oneSidedAUCAllNoDS4, validTestAllNoDS4] = createAUCMat(allTurnsNODSbeh4Neurons);
oneSidedAUCAllNoDS4(~validTestAllNoDS4) = NaN;

figure;
hold on;
cdfplot(oneSidedAUCrandomPop);
cdfplot(oneSidedAUCAllNoDS); % all turns, lumped together, treated as one.
cdfplot(oneSidedAUCAllNoDSHL);
cdfplot(oneSidedAUCAllNoDS4);
cdfplot(oneSidedAUCAllNoDS8);


allDataBehControl = [];
allDataBehControlLabels = [];

allDataBehControl = [allDataBehControl;oneSidedAUCAllNoDSHL];
allDataBehControlLabels = [allDataBehControlLabels;zeros(numel(oneSidedAUCAllNoDSHL),1)+1];

allDataBehControl = [allDataBehControl;oneSidedAUCAllNoDS4];
allDataBehControlLabels = [allDataBehControlLabels;zeros(numel(oneSidedAUCAllNoDS4),1)+2];

allDataBehControl = [allDataBehControl;oneSidedAUCAllNoDS8];
allDataBehControlLabels = [allDataBehControlLabels;zeros(numel(oneSidedAUCAllNoDS8),1)+3];

[pKW,anovaKW, STATS] = kruskalwallis(allDataBehControl,allDataBehControlLabels);
% pKW = 0.078922
% {'Source'}    {'SS'        }    {'df' }    {'MS'        }    {'Chi-sq'  }    {'Prob>Chi-sq'}
% {'Groups'}    {[3.8979e+04]}    {[  2]}    {[1.9490e+04]}    {[  5.0786]}    {[     0.0789]}
% {'Error' }    {[2.2789e+06]}    {[300]}    {[7.5965e+03]}    
% {'Total' }    {[2.3179e+06]}    {[302]}    
    
    
    
    