%% Olson ... Nitz MPC Paper
% Figure 1b - tracking example - colors were changed
% iRec = 12;
iRec = 25; % Animal 66
pathLM = MPCRecStruct_Final.pathRunsLineMarkers{iRec};

for iPath = 1:10
    figure;
    ax = gca;
    hold on;
    for iRun = 1:size(pathLM{iPath,1},1)
        ax.ColorOrderIndex = iPath; % I must have changed the colormap - not sure where I stored that.
        xs = MPCRecStruct_Final.processedDVT{iRec}(pathLM{iPath,1}(iRun):pathLM{iPath,2}(iRun),2);
        ys = MPCRecStruct_Final.processedDVT{iRec}(pathLM{iPath,1}(iRun):pathLM{iPath,2}(iRun),3);
        badSpots = xs<50 | xs>450 | ys<50 | ys>330; % remove off track from plotting
        badSpots = badSpots | [0; abs(diff(xs)) > 25 | abs(diff(ys)) > 25 ]; % remove jumps from glare from plotting.
%         disp(sum(badSpots));
        plot(xs(~badSpots),ys(~badSpots));
%         plot(xs,ys);

    end
%     axis([50 450 50 350]);
    axis off
end

%% All data, overlapped, to get full track plot.
figure;
ax = gca;
hold on;
for iPath = 1:10
    for iRun = 1:size(pathLM{iPath,1},1)
        ax.ColorOrderIndex = iPath; % I must have changed the colormap - not sure where I stored that.
        xs = MPCRecStruct_Final.processedDVT{iRec}(pathLM{iPath,1}(iRun):pathLM{iPath,2}(iRun),2);
        ys = MPCRecStruct_Final.processedDVT{iRec}(pathLM{iPath,1}(iRun):pathLM{iPath,2}(iRun),3);
        badSpots = xs<50 | xs>450 | ys<50 | ys>330; % remove off track from plotting
        badSpots = badSpots | [0; abs(diff(xs)) > 25 | abs(diff(ys)) > 25 ]; % remove jumps from glare from plotting.
%         disp(sum(badSpots));
        plot(xs(~badSpots),ys(~badSpots),'k');
%         plot(xs,ys);

    end
%     axis([50 450 50 350]);
    axis off
end
