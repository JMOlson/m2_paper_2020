%% Olson ... Nitz MPC Paper
% Figure 1DE - Example Neurons

% iNeu = 46; % l plan - middle example NS14, 
% iNeu = 77; % l action - left example DN66
% iNeu = 28; % r action - right example  NS14

load('figureColormap.mat');
mapToUse = hotMapClipped;

% Neurons used: 46, 77, 28
neuronList = [46, 77, 28];

%   snippet to step through and look at all neurons 1 by 1 for example
for i = 1:numel(neuronList)
    iNeu = neuronList(i);

    [twoDRMaps] = twoDPathsRateMapper(MPCRecStruct_Final,MPCNeuronStruct_Final,1,1,1,iNeu);
    twoDRMapsFiltered = filter2DMatrices(twoDRMaps,0);
    
    % Scale to value at 10% percentile of FRs sampled as max (otherwise max washes
    % out differences
    nGoodPoints = numel(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))));
    goodPointsOrdered = sort(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))),'descend');
    cMax = round(goodPointsOrdered(round(0.01*nGoodPoints)));
    
    % Plot 2D paths - each separately
    for iPath = 1:10
        figure;
        sc(twoDRMapsFiltered(:,:,iPath),[0,cMax],mapToUse,'w',isnan(twoDRMapsFiltered(:,:,iPath)));
    end
    
    % Plot perievent 2D ratemaps around turns
    plotResults = perieventPlotter( 'action', 'sequence', '12345', iNeu, TurnRasters, TurnParameters, ...
    MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10, 'std');
   % possible plot types
    %       'indData' - all individual traversals
    %       'meanOnly' - just avg of each group
    %       'sem' - means plus std err of the means
    %       'std' - means plus std deviation of the samples
    %       '95CI' - means plus 95% conf interval of means (bootstrapped)
    %       '99CI' -  - means plus 99% conf interval of means (bootstrapped)

    % print out choice probability value
    disp(oneSidedAUCAllNoDS(iNeu));
    
    disp('Color Axis Max is: ');
    disp(cMax);
end