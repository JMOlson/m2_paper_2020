%% Figure 2 Time Analyses of Actions
%% setup
% should be run as part of analysis instructions - if not, may need some
% other things - mostly just the one CP dataset

mapToUse = 'parula';

%% analyses
nBins = size(firstTurnTimeAnalysis.validTest,3);
startPoint = firstTurnTimeAnalysis.startPoint;
endPoint = firstTurnTimeAnalysis.endPoint;
timeStep = firstTurnTimeAnalysis.timeStepSize;

oneSidedAUCTimeAnalysis = abs(squeeze(firstTurnTimeAnalysis.auc)-0.5)+0.5;
oneSidedAUCShuffledTimeAnalysis = abs(squeeze(firstTurnTimeAnalysis.shuffledAuc)-0.5)+0.5;

validDataNeurons = sum(squeeze(firstTurnTimeAnalysis.validTest),2)==nBins; % 61 is nTotal Bins.
oneSidedAUCTimeAnalysisCleaned = oneSidedAUCTimeAnalysis(validDataNeurons,:);
oneSidedAUCShuffledTimeAnalysisCleaned = oneSidedAUCShuffledTimeAnalysis(validDataNeurons,:);

noFiringNeurons = sum(oneSidedAUCTimeAnalysisCleaned,2)==nBins*0.5;

% Same steps, but with Bootstrap values
oneSidedAUCBSTimeAnalysis = abs(squeeze(firstTurnTimeAnalysis.bsAUC)-0.5)+0.5;
sortedBSAUC = sort(oneSidedAUCBSTimeAnalysis,3);
nPermutations = firstTurnTimeAnalysis.nPermutations;
oneSidedAUCBS95TimeAnalysis = sortedBSAUC(:,:,nPermutations*0.95);
oneSidedAUCBS99TimeAnalysis = sortedBSAUC(:,:,nPermutations*0.99);
oneSidedAUCBS95TimeAnalysisCleaned = oneSidedAUCBS95TimeAnalysis(validDataNeurons,:);
oneSidedAUCBS99TimeAnalysisCleaned  = oneSidedAUCBS99TimeAnalysis(validDataNeurons,:);
nValidNeurons = sum(validDataNeurons);
% Neurons in recording order
% figure;
% sc(oneSidedAUCTimeAnalysisCleaned,[0.5,1],'parula');
% figure;
% sc(oneSidedAUCShuffledTimeAnalysisCleaned,[0.5,1],'parula');
% figure;
% sc(oneSidedAUCBS95TimeAnalysisCleaned,[0.5,1],'parula');

%% FIGURE 2A
% order neurons by timing of peak descriminability in the action sequence -
[~, maxInd] = max(oneSidedAUCTimeAnalysisCleaned,[],2);
[~,timeAnalIndexInMaxValOrder] = sort(maxInd);
timeAnalIndexInMaxValOrder = [timeAnalIndexInMaxValOrder(~any(bsxfun(@eq,...
    timeAnalIndexInMaxValOrder,find(noFiringNeurons)'),2));...
    find(noFiringNeurons)]; % puts nonfiring neurons at bottom of graph
figure;
sc(oneSidedAUCTimeAnalysisCleaned(timeAnalIndexInMaxValOrder,:),[0.5,1],mapToUse);
% figure;
% sc(oneSidedAUCShuffledTimeAnalysisCleaned(timeAnalIndexInMaxValOrder,:),[0.5,1],'parula');
% figure;
% sc(oneSidedAUCBS95TimeAnalysisCleaned(timeAnalIndexInMaxValOrder,:),[0.5,1],'parula');
% figure;
% sc(oneSidedAUCBS99TimeAnalysisCleaned(timeAnalIndexInMaxValOrder,:),[0.5,1],'parula');

% order by shuffled data, 
% [~, maxIndShuffled] = max(oneSidedAUCShuffledTimeAnalysisCleaned(~noFiringNeurons,:),[],2);
% [~,timeAnalIndexInMaxValOrderShuffled] = sort(maxIndShuffled);
% timeAnalIndexInMaxValOrderShuffled = [timeAnalIndexInMaxValOrderShuffled;find(noFiringNeurons)];
% figure;
% subplot(121)
% sc(oneSidedAUCTimeAnalysisCleaned(timeAnalIndexInMaxValOrderShuffled,:),[0.5,1],'parula','k',...
%     isnan(oneSidedAUCTimeAnalysisCleaned(timeAnalIndexInMaxValOrderShuffled,:)));
% subplot(122)
% sc(oneSidedAUCShuffledTimeAnalysisCleaned(timeAnalIndexInMaxValOrderShuffled,:),[0.5,1],'parula','k',...
%     isnan(oneSidedAUCShuffledTimeAnalysisCleaned(timeAnalIndexInMaxValOrderShuffled,:)));

figure;
hold on;
plot([startPoint:timeStep:endPoint],sum(oneSidedAUCTimeAnalysisCleaned>oneSidedAUCBS95TimeAnalysisCleaned,1));
plot([startPoint:timeStep:endPoint],sum(oneSidedAUCTimeAnalysisCleaned>oneSidedAUCBS99TimeAnalysisCleaned,1));
plot([startPoint:timeStep:endPoint],sum(oneSidedAUCShuffledTimeAnalysisCleaned>oneSidedAUCBS95TimeAnalysisCleaned,1));
legend('above 95%BS','above99%BS','sh above 95%')
plot([startPoint endPoint],[nValidNeurons*0.05,nValidNeurons*0.05],'k');

figure;
hold on;
plot([startPoint:timeStep:endPoint],nanmean(oneSidedAUCTimeAnalysisCleaned>oneSidedAUCBS95TimeAnalysisCleaned,1));
plot([startPoint:timeStep:endPoint],nanmean(oneSidedAUCTimeAnalysisCleaned>oneSidedAUCBS99TimeAnalysisCleaned,1));
plot([startPoint:timeStep:endPoint],nanmean(oneSidedAUCShuffledTimeAnalysisCleaned>oneSidedAUCBS95TimeAnalysisCleaned,1));
legend('above 95%BS','above99%BS','sh above 95%')
plot([startPoint endPoint],[0.05,0.05],'k');
axis([startPoint endPoint 0 0.6]);

figure;
hold on;
plot([startPoint:timeStep:endPoint],nanmean(oneSidedAUCTimeAnalysisCleaned>2/3,1));
plot([startPoint:timeStep:endPoint],nanmean(oneSidedAUCTimeAnalysisCleaned>0.9,1));
plot([startPoint:timeStep:endPoint],nanmean(oneSidedAUCShuffledTimeAnalysisCleaned>2/3,1));
plot([startPoint:timeStep:endPoint],nanmean(oneSidedAUCShuffledTimeAnalysisCleaned>0.9,1));
legend('above 66%','above90%','sh above 66%','sh above90%')
axis([startPoint endPoint 0 0.6]);

%% This appears to look at sequence turn 2, but all the way back to include
% the turn 1 window. Would need a whold different run of data though, can't
% just be uncommented.
% figure;
% hold on;
% plot([-100:10],nanmean(oneSidedAUCTimeAnalysisCleaned>2/3,1));
% plot([-100:10],nanmean(oneSidedAUCTimeAnalysisCleaned>0.9,1));
% plot([-100:10],nanmean(oneSidedAUCShuffledTimeAnalysisCleaned>2/3,1));
% plot([-100:10],nanmean(oneSidedAUCShuffledTimeAnalysisCleaned>0.9,1));
% legend('above 66%','above90%','sh above 66%','sh above90%')
% plot([-67,-67],[0,0.5],'r')
% plot([-31,-31],[0,0.5],'r')

%% Figure 2C
figure;
indNeuronMaxAUC = max(oneSidedAUCTimeAnalysisCleaned,[],2);
indNeuronNearMaxAUCBins = sum(oneSidedAUCTimeAnalysisCleaned>...
    repmat(0.9*indNeuronMaxAUC,1,nBins),2);
plot(indNeuronMaxAUC,indNeuronNearMaxAUCBins,'.','MarkerSize',20);

% figure;
% plot(sum(~isnan(oneSidedAUCTimeAnalysisCleaned),1));
% hold on;
% plot(sum(~isnan(oneSidedAUCShuffledTimeAnalysisCleaned),1));