%% Figure 4 Example Choice neuron
load([projectFolder,'\Data\AnalysisResults_CurrBioRev\TurnAlignedSpaceRasters.mat']);
iNeu = 207; % example fig 4

[twoDRMaps] = twoDPathsRateMapper(MPCRecStruct_Final,MPCNeuronStruct_Final,1,1,1,iNeu);
twoDRMapsFiltered = filter2DMatrices(twoDRMaps,0);

% Scale to value at 10% percentile of FRs sampled as max (otherwise max washes
% out differences
nGoodPoints = numel(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))));
goodPointsOrdered = sort(twoDRMapsFiltered(~isnan(twoDRMapsFiltered(:))),'descend');
cMax = round(goodPointsOrdered(round(0.01*nGoodPoints)));

% Plot 2D paths - each separately
for iPath = 1:10
    figure;
    sc(twoDRMapsFiltered(:,:,iPath),[0,cMax],mapToUse,'w',isnan(twoDRMapsFiltered(:,:,iPath)));
end

% Plot perievent 2D ratemaps around turns

% action discriminability at choice turns and forced turns separately.
plotResults = perieventPlotter( 'action', 'choice', '', iNeu, TurnRasters, TurnParameters, ...
    MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10, 'meanOnly');

% choice context (forced/choice) discriminability for the same actions (lefts), or (rights).
plotResults = perieventPlotter( 'context', 'choice', '', iNeu, TurnRasters, TurnParameters, ...
    MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10, 'meanOnly');

% possible plot types
%       'indData' - all individual traversals
%       'meanOnly' - just avg of each group
%       'sem' - means plus std err of the means
%       'std' - means plus std deviation of the samples
%       '95CI' - means plus 95% conf interval of means (bootstrapped)
%       '99CI' -  - means plus 99% conf interval of means (bootstrapped)

% print out choice probability value
oneSidedAUCCatAllConds(iNeu,:)

figure;
bar([oneSidedAUCCatAllMinConds(iNeu,:);...
    oneSidedAUCCatAllConds(iNeu,:);...
    oneSidedAUCCatAllMaxConds(iNeu,:)]);
legend({'L/R Action', 'Location', 'Progress', 'Orientation', 'Route', 'Choice'});
axis([0.5,3.5,0.5,1])

disp('Color Axis Max is: ');
disp(cMax);