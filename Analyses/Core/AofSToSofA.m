function [SofA] = AofSToSofA(AofS)
%AOFSTOSOFA Convert Array of Structs to a Structure of Arrays
%
% 	[SofA] = AofSToSofA(AofS)
%
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:%
% CREATED WITH MATLAB VERSION: 9.6.0.1174912 (R2019a) Update 5 on Microsoft Windows 10 Enterprise Version 10.0 (Build 18362)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: https://www.jmolson.com
% CREATED ON: 25-Nov-2019
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fieldList = fieldnames(AofS);
nFields = length(fieldList);
for iField = 1:nFields
    tmpA = cell(length(AofS),1);
    catDim = length(size(AofS(1).(fieldList{iField})))+1;
    [tmpA{:}] = deal(AofS.(fieldList{iField}));
    SofA.(fieldList{iField}) = cat(catDim,tmpA{:});
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
