function [ percentCleanRuns ] = cleanRunPercent( RecStruct )
%CLEANRUNPERCENT Calculate percent of runs scored as clean.
%   In each indRecStruct, clean runs are scored 1:10, dirty, -1:-10.
%   Written by Jake Olson, April 2017

recListIndices = unique(RecStruct.sourceFile);
numRecs = numel(recListIndices);

for iRec = 1:numRecs
    load(recListIndices{iRec});
    nClean = sum(indRecStruct.events(:,3)>0 & indRecStruct.events(:,3) <=10);
    nDirty = sum(indRecStruct.events(:,3)<0 & indRecStruct.events(:,3) >=-10);
    percentCleanRuns(iRec) = nClean /(nClean+nDirty);
end

ratNumbers = unique(RecStruct.rat);
for iRat = 1:numel(ratNumbers)
    nRecsEachRat(iRat) = numel(unique(RecStruct.rec(RecStruct.rat==ratNumbers(iRat))));
end
end

