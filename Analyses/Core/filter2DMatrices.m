function [ outputMaps ] = filter2DMatrices( inputMaps, downsampledFlag )
%FILTER2DMATRICES Applies smoothing to 2d matrices
%   Used in MPC and Sub papers
%   convolves using the nanconv function
%
% Written by Jake Olson, December 2015 updated april 2017

%2D ratemap smoothing function **************************************
if downsampledFlag
    halfNarrow = 5;
    narrowStdev = 2;
else
    halfNarrow = 7;
    narrowStdev = 3.5;
end
[xGridVals, yGridVals]=meshgrid(-halfNarrow:1:halfNarrow);
narrowGaussian = exp(-0.5 .* (xGridVals.^2+yGridVals.^2)/narrowStdev^2)/(narrowStdev*(sqrt(2*pi)));
narrowGaussianNormed=narrowGaussian./sum(sum(narrowGaussian));

for i = 1:size(inputMaps,3)
    for j = 1:size(inputMaps,4)
        %     different way of handling nans.
        %     outputMaps(:,:,i) = conv2nan(inputMaps(:,:,i,1),narrowGaussianNormed);
        
        outputMaps(:,:,i,j) = nanconv(inputMaps(:,:,i,j),narrowGaussianNormed, 'nanout');
    end
end
end

