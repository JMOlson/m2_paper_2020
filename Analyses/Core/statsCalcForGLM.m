function ModelStats = statsCalcForGLM(y, yHat, nStatShuffles)
%% all statistics calculations for error of the models.

% There can be nans in the yHats if there is no training data for a category. I'll ignore these
% situations.
ModelStats.mae = nanmean(abs(y - yHat));
ModelStats.mse = nanmean((y - yHat).^2);
ModelStats.mseBaseline = nanmean((y - nanmean(y)).^2);
ModelStats.r2_coeffOfDet = 1-(ModelStats.mse/ModelStats.mseBaseline);

%Do shuffling for stats baseline - If doing any normalization with the yHats. This doesn't really
%make sense for our categorical data, so I'm not doing any of it.
% mae_shuffled = nan(nStatShuffles,1);
% mse_shuffled = nan(nStatShuffles,1);
% r2_coeffOfDet_shuffled = nan(nStatShuffles,1);
% for iStatShuffle = 1:nStatShuffles
%     yHat_shuffled = yHat(randperm(length(yHat)));
%     mae_shuffled(iStatShuffle) = nanmean(abs(y - yHat_shuffled));
%     mse_shuffled(iStatShuffle) = nanmean((y - yHat_shuffled).^2);
%     r2_coeffOfDet_shuffled(iStatShuffle) = 1-(mse_shuffled(iStatShuffle)/ModelStats.mseBaseline);
% end
% 
% ModelStats.mae_shuffled = mae_shuffled;
% ModelStats.mse_shuffled = mse_shuffled;
% ModelStats.r2_coeffOfDet_shuffled = r2_coeffOfDet_shuffled;
end