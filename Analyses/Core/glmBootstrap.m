function [Results] = glmBootstrap(glmReadyData, bootstrapType, stat_function, predictorList, predictorIsCategorical, responseVar, responseDist, foldCount, nStatShuffles, nFullDataShuffles)
%GLMBOOTSTRAP Wrapper function to run GLM and bootstrap stats for M2_CurrBio_2020 paper
%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 17134)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: https://www.jmolson.com
% CREATED ON: 24-Nov-2019
% LAST MODIFIED BY:
% LAST MODIFIED ON:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Input Handling
if ~exist('nStatShuffles','var')
    nStatShuffles = 100;
end
if ~exist('nFullDataShuffles','var')
    nFullDataShuffles = 100;
end
if ~exist('foldCount','var')
    foldCount = 10;
end

switch bootstrapType
    case 'loocv'
    case 'kfold'
    otherwise
end
% turn off GLM warnings:
% Warning: Regression design matrix is rank deficient to within machine precision.
% Warning: Iteration limit reached.
% Warning: Weights are ill-conditioned. Data may be badly scaled, or the link function may be inappropriate.
warning off stats:LinearModel:RankDefDesignMat
warning off stats:glmfit:IterationLimit
warning off stats:glmfit:BadScaling


%% glm fit all. poisson w/ log linking fn.
nCells = length(glmReadyData);
for iCell = 1:nCells
    warning off all;
    tic;
    nDatapoints = size(glmReadyData{iCell},1);
    switch bootstrapType
        case 'loocv'
            maxIterations = nDatapoints;
            trainTestLabels = [];
        case 'kfold'
            trainTestLabels = cvpartition(nDatapoints,'kfold',foldCount);
            maxIterations = foldCount;
        otherwise
    end
    
    %% Train & test actual models
    [ModelStats(iCell), glmArray{iCell}] = trainAndTestModel(glmReadyData{iCell}, bootstrapType,...
        stat_function, predictorList, predictorIsCategorical, responseVar, responseDist,...
        nStatShuffles, maxIterations, trainTestLabels);
    
    %% Shuffle original data, repeat above.
    dataForShuffle = glmReadyData{iCell}; % done this way ease memory load on parfor loop below
    % parfor loop - remove par to debug
    parfor iShuffle = 1:nFullDataShuffles
        warning off all;
        shuffledGlmReadyTurnData = dataForShuffle;
        shuffledGlmReadyTurnData(:,~strcmp(dataForShuffle.Properties.VariableNames,responseVar)) = dataForShuffle(...
            randperm(nDatapoints),~strcmp(dataForShuffle.Properties.VariableNames,responseVar));
        ModelStats_Shuffled(iCell,iShuffle) = trainAndTestModel(shuffledGlmReadyTurnData, bootstrapType,...
            stat_function, predictorList, predictorIsCategorical, responseVar, responseDist,...
            nStatShuffles, maxIterations, trainTestLabels);
    end
    fprintf('Neuron %i of %i finshed after %0.5g seconds\n',iCell, nCells, toc);
end
Results.ModelStats = ModelStats;
Results.ModelStats_Shuffled = ModelStats_Shuffled;
Results.glmArray = glmArray;
end

function [ModelStats, glmArray] = trainAndTestModel(dataset, bootstrapType, stat_function,...
    predictorList, predictorIsCategorical, responseVar, responseDist, nStatShuffles, maxIterations, trainTestLabels)
for k = 1:maxIterations
    switch bootstrapType
        case 'loocv'
            isTrain = 1:maxIterations;
            isTrain(k) = [];
            isTest = k;
        case 'kfold'
            isTrain = trainTestLabels.training(k);
            isTest = trainTestLabels.test(k);
        otherwise
    end
    % train full model
    glm = fitglm(dataset(isTrain,:), 'Distribution', responseDist, 'ResponseVar', responseVar,...
        'PredictorVars', predictorList, 'CategoricalVars', predictorIsCategorical);
    %predict test data
    yHat = predict(glm, dataset(isTest,:));
    %% Calculate statistics for the model
    tmpStruct(k) = stat_function(dataset(isTest,:).(responseVar), yHat, nStatShuffles);
    glmArray{k} = glm;
end
structFieldNames = fieldnames(tmpStruct);
for iField = 1:length(structFieldNames)
    contentsSize = size(tmpStruct(1).(structFieldNames{iField}));
    ModelStats.(structFieldNames{iField}) = ...
        reshape([tmpStruct(:).(structFieldNames{iField})],[maxIterations,contentsSize]);
end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
