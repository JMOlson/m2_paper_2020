
%% Create list of all turns and their identifying parameters
[TurnRasters, TurnParameters, TurnPosition3D] = grabTurnData(MPCNeuronStruct_Final, MPCRecStruct_Final, -20, 10);

nCells = length(TurnRasters);
for iCell = 1:nCells
    TurnParameters{iCell}.meanFR = nanmean(TurnRasters{iCell},1)';
    TurnParameters{iCell}.logMeanFR = log(nanmean(TurnRasters{iCell},1))';
    
    glmReadyTurnData{iCell} = table(TurnParameters{iCell}.action, TurnParameters{iCell}.location,...
        TurnParameters{iCell}.progress, TurnParameters{iCell}.orientation,...
        TurnParameters{iCell}.route, TurnParameters{iCell}.choice, TurnParameters{iCell}.meanFR);
    glmReadyTurnData{iCell}.Properties.VariableNames = {'action'; 'location'; 'progress'; 'orientation'; 'route'; 'choice';'meanFR'};
end
% save TurnAlignedSpaceRasters TurnRasters TurnParameters TurnPosition3D

%% Define Parameters.
predictorList = {'action'; 'location'; 'progress'; 'orientation'; 'route'; 'choice'};
predictorIsCategorical = {'action'; 'location'; 'progress'; 'orientation'; 'route'; 'choice'};

%% Run single predictor GLM
foldCount = 10;
nStatShuffles = 100;
nFullDataShuffles = 100;
for iPredictor = 1:length(predictorList)
    onePredictorGLMResults(iPredictor) = glmBootstrap(glmReadyTurnData, 'kfold', @statsCalcForGLM,...
        predictorList(iPredictor), predictorIsCategorical(iPredictor), 'meanFR', 'poisson',...
        foldCount, nStatShuffles, nFullDataShuffles);
end
% save GLMOnePredictorModels onePredictorGLMResults predictorList predictorIsCategorical

%% Run full model GLM
foldCount = 10;
nStatShuffles = 100;
nFullDataShuffles = 100;
fullGLMResults = glmBootstrap(glmReadyTurnData, 'kfold', @statsCalcForGLM,...
    predictorList, predictorIsCategorical, 'meanFR', 'poisson',...
    foldCount, nStatShuffles, nFullDataShuffles);
% save GLMfullModels fullGLMResults predictorList predictorIsCategorical