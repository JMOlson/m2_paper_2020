function Results = statsCalc_postglm(modelResults)
%% Initiialization
nCells = length(modelResults.ModelStats);
nShuffles = size(modelResults.ModelStats_Shuffled,2);
nFold = size(modelResults.ModelStats(1).mae,1);

%% Get model criteria values.
modelCriteria = fieldnames(modelResults.glmArray{1}{1}.ModelCriterion); 
nModelCriteria = length(modelCriteria);
modelCriteriaResults = cell(nModelCriteria,nCells);
for iFieldName = 1:nModelCriteria
    modelCriteriaResults(iFieldName,:) = [cellfun(@(x) [cellfun(@(y) y.ModelCriterion.(modelCriteria(iFieldName)),x)], modelResults.glmArray,'UniformOutput',false)];
end

% avg over nFolds of model runs to get the mean model fitness for this neuron.
Results.modelCriteriaResults_mean = cellfun(@(x) mean(x),modelCriteriaResults); 
Results.modelCriteria = modelCriteria;

% tC is short for thisCell - i'm iterating through.
for iCell = 1:nCells
    tC_ModelStats = modelResults.ModelStats(iCell);
    tC_ModelStats_Shuffled = modelResults.ModelStats_Shuffled(iCell,:);
    tC_glmArray = modelResults.glmArray{iCell};
    
    Results.modelSigFit_mae_ModelShuffle(iCell) = mean(tC_ModelStats.mae);
     
    % Reshape data into matrices collapsing across shuffles.
    tC_ModelStats_Shuffled_Collapsed = AofSToSofA(tC_ModelStats_Shuffled);
   
    % prediction shuffle dist CIs
%     sortedErr = sort(tC_ModelStats.mae_shuffled,2,'descend');
%     tC_95CI_yHatShuffle_MAE = sortedErr(:,95);
%     sortedErr = sort(tC_ModelStats.mse_shuffled,2,'descend');
%     tC_95CI_yHatShuffle_MSE = sortedErr(:,95);
%     sortedErr = sort(tC_ModelStats.r2_coeffOfDet_shuffled,2,'ascend');
%     tC_95CI_yHatShuffle_R2 = sortedErr(:,95);

%     GainShuffled_MAE = squeeze(mean(tC_ModelStats_Shuffled_Collapsed.mae_shuffled,2) ./ tC_ModelStats_Shuffled_Collapsed.mae);
%     sortedErr = sort(GainShuffled_MAE,2,'ascend');
%     tC_95CI_gain_MAE = sortedErr(:,95);
%     GainShuffled_MSE = squeeze(mean(tC_ModelStats_Shuffled_Collapsed.mse_shuffled,2) ./ tC_ModelStats_Shuffled_Collapsed.mse);
%     sortedErr = sort(GainShuffled_MSE,2,'ascend');
%     tC_95CI_gain_MSE = sortedErr(:,95);
  
%     GainActual_MAE = mean(tC_ModelStats.mae_shuffled,2) ./ tC_ModelStats.mae;
%     GainActual_MSE = mean(tC_ModelStats.mse_shuffled,2) ./ tC_ModelStats.mse;
    
    
    % Significance (> 95th percentile) w.r.t. shuffled predictions
%     Results.modelSigFit_mae_ResultShuffle95_sig(iCell,:) = tC_95CI_yHatShuffle_MAE > tC_ModelStats.mae;
%     Results.modelSigFit_mse_ResultShuffle95_sig(iCell,:) = tC_95CI_yHatShuffle_MSE > tC_ModelStats.mse;
%     Results.modelSigFit_r2_ResultShuffle95_sig(iCell,:) = tC_95CI_yHatShuffle_R2 < tC_ModelStats.r2_coeffOfDet;
%     Results.modelSigFit_mae_ResultShuffle95_sig_mean(iCell) = mean(tC_95CI_yHatShuffle_MAE) > mean(tC_ModelStats.mae);
%     Results.modelSigFit_mse_ResultShuffle95_sig_mean(iCell) = mean(tC_95CI_yHatShuffle_MSE) > mean(tC_ModelStats.mse);
%     Results.modelSigFit_r2_ResultShuffle95_sig_mean(iCell) = mean(tC_95CI_yHatShuffle_R2) < mean(tC_ModelStats.r2_coeffOfDet);
    
%     Results.modelSigFit_mae_Gain95_sig(iCell,:) = tC_95CI_gain_MAE < GainActual_MAE;
%     Results.modelSigFit_mse_Gain95_sig(iCell,:) = tC_95CI_gain_MSE < GainActual_MSE;
%     Results.modelSigFit_mae_Gain95_sig_mean(iCell) = mean(tC_95CI_gain_MAE) < mean(GainActual_MAE);
%     Results.modelSigFit_mse_Gain95_sig_mean(iCell) = mean(tC_95CI_gain_MSE) < mean(GainActual_MSE);
    

    % shuffled input ys error distribution
     sortedErr = sort(squeeze(tC_ModelStats_Shuffled_Collapsed.mae),2,'descend');
     tC_95CI_modelShuffle_MAE = sortedErr(:,95);
%     sortedErr = sort(squeeze(tC_ModelStats_Shuffled_Collapsed.mse),2,'descend');
%     tC_95CI_modelShuffle_MSE = sortedErr(:,95);
%     sortedErr = sort(squeeze(tC_ModelStats_Shuffled_Collapsed.r2_coeffOfDet),2,'ascend');
%     tC_95CI_modelShuffle_R2 = sortedErr(:,95);
    
    % Significance (> 95th percentile) w.r.t. shuffled input ys
    Results.modelSigFit_mae_ModelShuffle95(iCell) = mean(tC_95CI_modelShuffle_MAE);
    Results.modelSigFit_mae_ModelShuffle95_sig(iCell,:) = tC_95CI_modelShuffle_MAE > tC_ModelStats.mae;
%     Results.modelSigFit_mse_ModelShuffle95(iCell,:) = tC_95CI_modelShuffle_MSE > tC_ModelStats.mse;
%     Results.modelSigFit_r2_ModelShuffle95(iCell,:) = tC_95CI_modelShuffle_R2 < tC_ModelStats.r2_coeffOfDet;
    

    Results.modelSigFit_mae_ModelShuffle95_sigMean(iCell) = mean(tC_95CI_modelShuffle_MAE) > mean(tC_ModelStats.mae);
%     Results.modelSigFit_mse_ModelShuffle95_sig(iCell) = mean(tC_95CI_modelShuffle_MSE) > mean(tC_ModelStats.mse);
%     Results.modelSigFit_r2_ModelShuffle95_sig(iCell) = mean(tC_95CI_modelShuffle_R2) < mean(tC_ModelStats.r2_coeffOfDet);
%     fprintf('%% Sig w/ MAE vs shuffle models  = %g\n',mean(modelSigFit_mae_ModelSShuffle95(iCell,:)));
%     fprintf('%% Sig w/ MSE vs shuffle models = %g\n',mean(modelSigFit_mse_ModelSShuffle95(iCell,:)));
%     fprintf('%% Sig w/ R2 vs shuffle models = %g\n',mean(modelSigFit_r2_ModelSShuffle95(iCell,:)));

    
    % prediction shuffle dist CIs for shuffled models
    % not really important, just a sanity check - should be chance, and was
%     for iShuffle = 1:nShuffles
%         tC_ModelStats_oneShuffle = tC_ModelStats_Shuffled(iShuffle);
%         
%         sortedErr = sort(tC_ModelStats_oneShuffle.mae_shuffled,2,'descend');
%         tC_95CI_yHatShuffle_MAE = sortedErr(:,95);
%         sortedErr = sort(tC_ModelStats_oneShuffle.mse_shuffled,2,'descend');
%         tC_95CI_yHatShuffle_MSE = sortedErr(:,95);
%         sortedErr = sort(tC_ModelStats_oneShuffle.r2_coeffOfDet_shuffled,2,'ascend');
%         tC_95CI_yHatShuffle_R2 = sortedErr(:,95);
%         
%         Results.modelSigFit_mae_ResultShuffle95_Shuffled(iCell,:,iShuffle) = tC_95CI_yHatShuffle_MAE > tC_ModelStats_oneShuffle.mae;
%         Results.modelSigFit_mse_ResultShuffle95_Shuffled(iCell,:,iShuffle) = tC_95CI_yHatShuffle_MSE > tC_ModelStats_oneShuffle.mse;
%         Results.modelSigFit_r2_ResultShuffle95_Shuffled(iCell,:,iShuffle) = tC_95CI_yHatShuffle_R2 < tC_ModelStats_oneShuffle.r2_coeffOfDet;
%     end
%     fprintf('Shuffled Model %% Sig w/ MAE Result Shuffle = %g\n',mean(sum(squeeze(modelSigFit_mae_ResultShuffle95_Shuffled(iCell,:,:)),2))./nShuffles);
%     fprintf('Shuffled Model %% Sig w/ MSE Result Shuffle = %g\n',mean(sum(squeeze(modelSigFit_mse_ResultShuffle95_Shuffled(iCell,:,:)),2))./nShuffles);
%     fprintf('Shuffled Model %% Sig w/ R2 Result Shuffle = %g\n',mean(sum(squeeze(modelSigFit_r2_ResultShuffle95_Shuffled(iCell,:,:)),2))./nShuffles);
   
end


end