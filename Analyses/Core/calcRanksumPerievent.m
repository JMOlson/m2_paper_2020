function Results = calcRanksumPerievent(groupType,indexType,turnIndices,...
    NeuronStruct,RecStruct,startPoint,endPoint,maxRuns,doBootstrap,indBinFlag,neuronsSelected,isTimeAnalysis, timeStepSize)
%% Main analysis for choice probability metric.

%% Includes subfunction findRasterRows which creates logical vectors of which turns have the correct parameters and should be analyzed.

%% Input Handling
if ~exist('neuronsSelected','var')
    neuronsSelected = true(length(NeuronStruct.rat),1);
elseif isempty(neuronsSelected)
    neuronsSelected = true(length(NeuronStruct.rat),1);
elseif length(neuronsSelected) == 1
    neuronsSelected = repmat(neuronsSelected,length(NeuronStruct.rat),1);
end

if ~exist('doBootstrap','var')
    doBootstrap = false;
end
if ~exist('indBinFlag','var')
    indBinFlag = false;
end
if ~exist('isTimeAnalysis','var')
    isTimeAnalysis = false;
end
if ~exist('timeStepSize','var')
    timeStepSize = 1/20; % animals run ~20cm/sec = this should give ~same bin size/counts as space.
end

%% Set constants, initialize variables.
MINRUNS = 8;
NPERMUTATIONS = 1000;
rng('shuffle');

if isTimeAnalysis
    nTotalBins = length(startPoint:timeStepSize:endPoint);
else
    nTotalBins = endPoint-startPoint+1;
end

nNeurons = sum(neuronsSelected);
if numel(maxRuns) == 1
    maxRuns = repmat(maxRuns,nNeurons,1);
end

Results.groupType = groupType;
Results.contextType = indexType;
Results.turnsIncluded = turnIndices;
Results.startPoint = startPoint;
Results.endPoint = endPoint;
Results.timeStepSize = timeStepSize;
Results.minRuns = MINRUNS;
Results.maxRuns = maxRuns;
Results.neuronsSelected = neuronsSelected;
Results.nPermutations = NPERMUTATIONS;

% Get parameters (path, turn indices) needed for each group
[group1, group2, Results.group1Names, Results.group2Names] = formGroups(groupType, indexType, turnIndices, RecStruct);

% Create turn rasters for all turns - grab selections of this to do the tests on.
[TurnRasters, TurnParameters] = grabTurnData(NeuronStruct, RecStruct, startPoint, endPoint, ~neuronsSelected,isTimeAnalysis, timeStepSize);

Results.group1 = group1;
Results.group2 = group2;

% cell array of comparisons vs individual comparison
if ~iscell(group1)
    nPairs = 1;
    isACell = 0;
else
    nPairs = size(group1,1);
    isACell = 1;
end

%% Initialize most variables --- not for context - those will be cells
Results.meanLFR = nan(nNeurons,nPairs);
Results.meanRFR = nan(nNeurons,nPairs);
Results.nEvents = nan(2,nNeurons,nPairs);
Results.nEventsUsed = nan(2,nNeurons,nPairs);
switch groupType
    case 'action'
        if indBinFlag
            Results.nEventsUsed = nan(2,nNeurons,nPairs,nTotalBins);
            Results.validTest = false(nNeurons,nPairs,nTotalBins);
            Results.ranksumP = nan(nNeurons,nPairs,nTotalBins);
            Results.ranksumResult = nan(nNeurons,nPairs,nTotalBins);
            Results.ranksumStats = nan(nNeurons,nPairs,nTotalBins);
            Results.shuffledRanksumP = nan(nNeurons,nPairs,nTotalBins);
            Results.shuffledRanksumResult = nan(nNeurons,nPairs,nTotalBins);
            Results.shuffledRanksumStats = nan(nNeurons,nPairs,nTotalBins);
            Results.bsRank = nan(nNeurons,nPairs,nTotalBins);
            Results.u = nan(nNeurons,nPairs,nTotalBins);
            Results.auc = nan(nNeurons,nPairs,nTotalBins);
            Results.shuffledU = nan(nNeurons,nPairs,nTotalBins);
            Results.shuffledAuc = nan(nNeurons,nPairs,nTotalBins);
            Results.bsU = nan(nNeurons,nPairs,nTotalBins,NPERMUTATIONS);
            Results.bsAuc = nan(nNeurons,nPairs,nTotalBins,NPERMUTATIONS);
        else
            Results.nEventsUsed = nan(2,nNeurons,nPairs);
            Results.validTest = false(nNeurons,nPairs);
            Results.ranksumP = nan(nNeurons,nPairs);
            Results.ranksumResult = nan(nNeurons,nPairs);
            Results.ranksumStats = nan(nNeurons,nPairs);
            Results.shuffledRanksumP = nan(nNeurons,nPairs);
            Results.shuffledRanksumResult = nan(nNeurons,nPairs);
            Results.shuffledRanksumStats = nan(nNeurons,nPairs);
            Results.bsRank = nan(nNeurons,nPairs);
            Results.u = nan(nNeurons,nPairs);
            Results.auc = nan(nNeurons,nPairs);
            Results.shuffledU = nan(nNeurons,nPairs);
            Results.shuffledAuc = nan(nNeurons,nPairs);
            Results.bsU = nan(nNeurons,nPairs,NPERMUTATIONS);
            Results.bsAuc = nan(nNeurons,nPairs,NPERMUTATIONS);
        end
end

%% main code
for iNeuron = 1:nNeurons
    iRec = NeuronStruct.recCount(iNeuron);
    
    % Grab data, take means, store for tests.
    for iPair = 1:nPairs
        if isACell && strcmp(indexType,'choice')
            group1PathTurn = group1{iPair,iRec};
            group2PathTurn = group2{iPair,iRec};
        elseif isACell
            group1PathTurn = group1{iPair,1};
            group2PathTurn = group2{iPair,1};
        else
            group1PathTurn = group1;
            group2PathTurn = group2;
        end
        
        relevantRows = findRasterRows(TurnParameters{iNeuron},group1PathTurn);
        group1FRsAll{iPair} = TurnRasters{iNeuron}(:,relevantRows);
        group1MeanFRsAll{iPair} = nanmean(group1FRsAll{iPair},1);
         
        relevantRows = findRasterRows(TurnParameters{iNeuron},group2PathTurn);
        group2FRsAll{iPair} = TurnRasters{iNeuron}(:,relevantRows);
        group2MeanFRsAll{iPair} = nanmean(group2FRsAll{iPair},1);
    end
    
    switch groupType
        case 'action'
            %test each pair to test:
            for iPair = 1:nPairs
                group1MeanFRs = group1MeanFRsAll{iPair};
                group2MeanFRs = group2MeanFRsAll{iPair};
                group1FRs = group1FRsAll{iPair};
                group2FRs = group2FRsAll{iPair};
                
                nGroup1 = numel(group1MeanFRs);
                nGroup2 = numel(group2MeanFRs);
                
                Results.meanLFR(iNeuron,iPair) = mean(group1MeanFRs);
                Results.meanRFR(iNeuron,iPair) = mean(group2MeanFRs);
                Results.nEvents(:,iNeuron,iPair) = [nGroup1;nGroup2];
                
                % Verify enough datapoints for valid analysis
                if indBinFlag
                    nGroup1Bins = sum(~isnan(group1FRs),2);
                    nGroup2Bins = sum(~isnan(group2FRs),2);
                    if isempty(nGroup1Bins) || isempty(nGroup2Bins)
                        Results.validTest(iNeuron,iPair,:) = false;
                        break;
                    else
                        Results.validTest(iNeuron,iPair,:) = (nGroup1Bins >= MINRUNS) & (nGroup2Bins >= MINRUNS);
                    end
                else
                    Results.validTest(iNeuron,iPair) = (nGroup1 >= MINRUNS) && (nGroup2 >= MINRUNS);
                end
                
                % subsampling if necessary, pass through otherwise - these are datasets to test
                if nGroup1 + nGroup2 > maxRuns(iNeuron)
                    group1Count = floor(maxRuns(iNeuron)/2);
                    group2Count = maxRuns(iNeuron)-group1Count;
                    if nGroup1 < group1Count
                        group1Count = nGroup1;
                        group2Count = maxRuns(iNeuron)-group1Count;
                    elseif nGroup2 < group2Count
                        group2Count = nGroup2;
                        group1Count = maxRuns(iNeuron)-group2Count;
                    end
                    if indBinFlag
                        group1DataToTest = group1FRs(randperm(nGroup1,group1Count),:);
                        group2DataToTest = group2FRs(randperm(nGroup2,group2Count),:);
                        Results.nEventsUsed(:,iNeuron,iPair,:) = reshape([sum(~isnan(group1DataToTest),2),sum(~isnan(group2DataToTest),2)]',2,1,1,nTotalBins);
                    else
                        group1DataToTest = group1MeanFRs(randperm(nGroup1,group1Count));
                        group2DataToTest = group2MeanFRs(randperm(nGroup2,group2Count));
                        Results.nEventsUsed(:,iNeuron,iPair) = [size(group1DataToTest,2);size(group2DataToTest,2)];
                    end
                else
                    if indBinFlag
                        group1DataToTest = group1FRs;
                        group2DataToTest = group2FRs;
                        Results.nEventsUsed(:,iNeuron,iPair,:) = reshape([sum(~isnan(group1DataToTest),2),sum(~isnan(group2DataToTest),2)]',2,1,1,nTotalBins);
                    else
                        group1DataToTest = group1MeanFRs;
                        group2DataToTest = group2MeanFRs;
                        Results.nEventsUsed(:,iNeuron,iPair) = [size(group1DataToTest,2);size(group2DataToTest,2)];
                    end
                end
                
                % Calculate CP - U Statistic & AUC (== CP)
                if indBinFlag
                    for iBin = 1:nTotalBins
                        if Results.validTest(iNeuron,iPair,iBin)
                            % shuffledTestEventCounts = zeros(2,nTotalBins);
                            [Results.ranksumP(iNeuron,iPair,iBin),...
                                Results.ranksumResult(iNeuron,iPair,iBin),...
                                statStruct] = ranksum(group1DataToTest(iBin,:),group2DataToTest(iBin,:));
                            Results.ranksumStats(iNeuron,iPair,iBin) = statStruct.ranksum;
                            %                     Results.u(iNeuron,iPair,:) = squeeze(Results.ranksumStats(iNeuron,iPair,:))-...
                            %                         squeeze(Results.nEventsUsed(1,iNeuron,iPair,:).*(Results.nEventsUsed(1,iNeuron,iPair,:)+1)./2);
                            %                     Results.auc(iNeuron,iPair,:) = squeeze(Results.u(iNeuron,iPair,:))./...
                            %                         squeeze(Results.nEventsUsed(1,iNeuron,iPair,:).*Results.nEventsUsed(2,iNeuron,iPair,:));
                            %
                            % Generate correct numbers of data points for each category
                            allData = [group1DataToTest(iBin,:),group2DataToTest(iBin,:)];
                            totalBSSamples = numel(allData);
                            nBSGroup1 = floor(totalBSSamples/2);
                            randomEventCounts(:,iNeuron,iPair,iBin) = [nBSGroup1,totalBSSamples-nBSGroup1];
                            % Generate one random value for this data -
                            % will use for control auc population.
                            allDataToTest = allData(randperm(totalBSSamples));
                            [Results.shuffledRanksumP(iNeuron,iPair,iBin),...
                                Results.shuffledRanksumResult(iNeuron,iPair,iBin),...
                                statStruct] = ...
                                ranksum(allDataToTest(1:nBSGroup1),allDataToTest(nBSGroup1+1:end));
                            Results.shuffledRanksumStats(iNeuron,iPair,iBin) = statStruct.ranksum;
                            %                     Results.shuffledU(iNeuron,iPair,:) = squeeze(Results.shuffledRanksumStats(iNeuron,iPair,:))-...
                            %                         (randomEventCounts(1,:).*(randomEventCounts(1,:)+1)./2)';
                            %                     Results.shuffledAuc(iNeuron,iPair,:) = squeeze(Results.shuffledU(iNeuron,iPair,:))./...
                            %                         (randomEventCounts(1,:).*randomEventCounts(2,:))';
                            %
                            % bootstrap portion
                            if doBootstrap
                                bootstrapRankSumP = nan(NPERMUTATIONS,1);
                                bsRanksumStats = nan(NPERMUTATIONS,1);
                                parfor iPermutation = 1:NPERMUTATIONS
                                    allDataToTest = allData(randperm(totalBSSamples));
                                    [bootstrapRankSumP(iPermutation),~,statStruct] = ...
                                        ranksum(allDataToTest(1:nBSGroup1),allDataToTest(nBSGroup1+1:end));
                                    bsRanksumStats(iPermutation) = statStruct.ranksum;
                                end
                                Results.bsRank(iNeuron,iPair,iBin) = sum(Results.ranksumP(iNeuron,iPair,iBin) <...
                                    bootstrapRankSumP);
                                Results.bsU(iNeuron,iPair,iBin,:) = bsRanksumStats-...
                                    (randomEventCounts(1,iNeuron,iPair,iBin).*(randomEventCounts(1,iNeuron,iPair,iBin)+1)./2)';
                                Results.bsAUC(iNeuron,iPair,iBin,:) = Results.bsU(iNeuron,iPair,iBin,:)./...
                                    (randomEventCounts(1,iNeuron,iPair,iBin).*randomEventCounts(2,iNeuron,iPair,iBin))';
                            end
                        end
                    end
                else
                    if Results.validTest(iNeuron,iPair)
                        [Results.ranksumP(iNeuron,iPair),...
                            Results.ranksumResult(iNeuron,iPair),...
                            statStruct] = ...
                            ranksum(group1DataToTest,group2DataToTest);
                        Results.ranksumStats(iNeuron,iPair) = statStruct.ranksum;
                        %                 Results.u(iNeuron,iPair) = Results.ranksumStats(iNeuron,iPair)-...
                        %                     (Results.nEventsUsed(1,iNeuron,iPair).*(Results.nEventsUsed(1,iNeuron,iPair)+1)./2);
                        %                 Results.auc(iNeuron,iPair) = Results.u{iNeuron,iPair}./...
                        %                     (Results.nEventsUsed(1,iNeuron,iPair).*Results.nEventsUsed(2,iNeuron,iPair));
                        %
                        % Generate correct numbers of data points for each category
                        allData = [group1MeanFRs,group2MeanFRs];
                        totalBSSamples = sum(Results.nEventsUsed(:,iNeuron,iPair));
                        nBSGroup1 = floor(totalBSSamples/2);
                        randomEventCounts(:,iNeuron,iPair) = [nBSGroup1,totalBSSamples-nBSGroup1];
                        % Generate one random value for this data -
                        % will use for control auc population.
                        allDataToTest = allData(randperm(nGroup1+nGroup2,totalBSSamples));
                        [Results.shuffledRanksumP(iNeuron,iPair),...
                            Results.shuffledRanksumResult(iNeuron,iPair),...
                            statStruct] = ranksum(allDataToTest(1:nBSGroup1),allDataToTest(nBSGroup1+1:end));
                        Results.shuffledRanksumStats(iNeuron,iPair) = statStruct.ranksum;
                        %                 Results.randomU(iNeuron,iPair) = Results.randomRanksumStats(iNeuron,iPair)-...
                        %                     (randomEventCounts(1,iNeuron,iPair).*(randomEventCounts(1,iNeuron,iPair)+1)./2);
                        %                 Results.randomAuc(iNeuron,iPair) = Results.randomU{iNeuron}(iPair)./...
                        %                     (randomEventCounts(1,iNeuron,iPair).*randomEventCounts(2,iNeuron,iPair));
                        %
                        % Bootstrap Procedure
                        if doBootstrap
                            bootstrapRankSumP = nan(NPERMUTATIONS,1);
                            bsRanksumStats = nan(NPERMUTATIONS,1);
                            for iPermutation = 1:NPERMUTATIONS
                                allDataToTest = allData(randperm(nGroup1+nGroup2,totalBSSamples));
                                [bootstrapRankSumP(iPermutation),~,statStruct] = ...
                                    ranksum(allDataToTest(1:nBSGroup1),allDataToTest(nBSGroup1+1:end));
                                bsRanksumStats(iPermutation) = statStruct.ranksum;
                            end
                            Results.bsRank(iNeuron,iPair) = sum(Results.ranksumP(iNeuron,iPair) <...
                                bootstrapRankSumP);
                            Results.bsU{iNeuron}(iPair,:) = bsRanksumStats-...
                                (randomEventCounts(1,iNeuron,iPair).*(randomEventCounts(1,iNeuron,iPair)+1)./2);
                            Results.bsAUC{iNeuron}(iPair,:) = Results.bsU{iNeuron}(iPair,:)./...
                                (randomEventCounts(1,iNeuron,iPair).*randomEventCounts(2,iNeuron,iPair));
                        end
                    end
                end
            end
        case 'context'
            % test 1 is lefts, test 2 is rights
            sizeGroup1s = cellfun(@numel, group1MeanFRsAll);
            sizeGroup2s = cellfun(@numel, group2MeanFRsAll);
            sizeOfGroups = {sizeGroup1s,sizeGroup2s};
            Results.meanLFR(iNeuron,:) = cellfun(@mean, group1MeanFRsAll);
            Results.meanRFR(iNeuron,:) = cellfun(@mean, group2MeanFRsAll);
            Results.nEvents(1,iNeuron,:) = sizeGroup1s;
            Results.nEvents(2,iNeuron,:) = sizeGroup2s;
            
            groupData = {group1MeanFRsAll,group2MeanFRsAll};
            if indBinFlag
                error('Action sequence analysis (bin by bin) not implemented for context');
            end
            for iAction = 1:2
                % No Subsample Implemented
                nSubGroups = numel(Results.nEvents(iAction,iNeuron,:));
                subGroupSizes = squeeze(Results.nEvents(iAction,iNeuron,:));
                if sum(sizeOfGroups{iAction}) > maxRuns(iNeuron)
                    error('Subsampling to match max run values not implemented for context');
                else
                    groupDataToTest = groupData{iAction};
                end
                Results.nEventsUsed(iAction,iNeuron,:) = cellfun(@numel,groupDataToTest);
                
                if nSubGroups > 1
                    nCombos = nchoosek(1:nSubGroups,2);
                    for iCombo = 1:size(nCombos,1)
                        Results.validTest{iAction,iNeuron}(iCombo) = all(subGroupSizes(nCombos(iCombo,:)) >= MINRUNS);
                        if Results.validTest{iAction,iNeuron}(iCombo)
                            % pairwise ranksum test
                            [Results.ranksumP{iAction,iNeuron}(iCombo),...
                                Results.ranksumResult{iAction,iNeuron}(iCombo),...
                                statStruct] = ...
                                ranksum(groupDataToTest{nCombos(iCombo,1)},...
                                groupDataToTest{nCombos(iCombo,2)});
                            Results.ranksumStats{iAction,iNeuron}(iCombo) = statStruct.ranksum;
                            % No Bootstrap implemented yet for this case
                            % AUC calc
                            Results.u{iAction,iNeuron}(iCombo) = Results.ranksumStats{iAction,iNeuron}(iCombo) -...
                                (Results.nEventsUsed(iAction,iNeuron,nCombos(iCombo,1)).*...
                                (Results.nEventsUsed(iAction,iNeuron,nCombos(iCombo,1))+1)./2);
                            Results.auc{iAction,iNeuron}(iCombo) = Results.u{iAction,iNeuron}(iCombo)./...
                                (Results.nEventsUsed(iAction,iNeuron,nCombos(iCombo,1)).*...
                                Results.nEventsUsed(iAction,iNeuron,nCombos(iCombo,2)));
                            
                            % shuffled
                            allData = [groupData{iAction}{nCombos(iCombo,1)},groupData{iAction}{nCombos(iCombo,2)}];
                            totalBSSamples = numel(allData);
                            nBSGroup1 = floor(totalBSSamples/2);
                            randomEventCounts = [nBSGroup1,totalBSSamples-nBSGroup1];
                            
                            % Generate one random value for this data -
                            % will use for control auc population.
                            allDataToTest = allData(randperm(totalBSSamples));
                            [Results.shuffledRanksumP{iAction,iNeuron}(iCombo),...
                                Results.shuffledRanksumResult{iAction,iNeuron}(iCombo),...
                                statStruct] = ...
                                ranksum(allDataToTest(1:nBSGroup1),allDataToTest(nBSGroup1+1:end));
                            Results.shuffledRanksumStats{iAction,iNeuron}(iCombo) = statStruct.ranksum;
                            
                            Results.shuffledU{iAction,iNeuron}(iCombo) = Results.shuffledRanksumStats{iAction,iNeuron}(iCombo) -...
                                (randomEventCounts(1).*(randomEventCounts(1)+1)./2);
                            Results.shuffledAuc{iAction,iNeuron}(iCombo) = Results.shuffledU{iAction,iNeuron}(iCombo)./...
                                (randomEventCounts(1).*randomEventCounts(2));
                        else
                            Results.ranksumP{iAction,iNeuron}(iCombo) = NaN;
                            Results.ranksumResult{iAction,iNeuron}(iCombo) = false;
                            Results.ranksumStats{iAction,iNeuron}(iCombo) = NaN;
                            Results.u{iAction,iNeuron}(iCombo) = NaN;
                            Results.auc{iAction,iNeuron}(iCombo) = NaN;
                            Results.shuffledRanksumP{iAction,iNeuron}(iCombo) = NaN;
                            Results.shuffledRanksumResult{iAction,iNeuron}(iCombo) = false;
                            Results.shuffledRanksumStats{iAction,iNeuron}(iCombo) = NaN;
                            Results.shuffledU{iAction,iNeuron}(iCombo) = NaN;
                            Results.shuffledAuc{iAction,iNeuron}(iCombo) = NaN;
                        end
                    end
                end
            end
    end
end
switch groupType
    case 'action'
        Results.u = Results.ranksumStats -...
            shiftdim(Results.nEventsUsed(1,:,:,:).*(Results.nEventsUsed(1,:,:,:)+1)./2,1);
        Results.auc = Results.u./...
            shiftdim(Results.nEventsUsed(1,:,:,:).*Results.nEventsUsed(2,:,:,:),1);
        Results.shuffledU = Results.shuffledRanksumStats-...
            shiftdim(randomEventCounts(1,:,:,:).*(randomEventCounts(1,:,:,:)+1)./2,1);
        Results.shuffledAuc = Results.shuffledU./...
            shiftdim(randomEventCounts(1,:,:,:).*randomEventCounts(2,:,:,:),1);
end
end

function logicalRowVector = findRasterRows(TurnParameters,turnIndices)
%turn indices are identified in [route,progress] pairs
logicalRowVector = false(size(TurnParameters,1),1);
nTurns = size(turnIndices,1);
for iTurn = 1:nTurns
logicalRowVector(TurnParameters.route == turnIndices(iTurn,1) &...
    TurnParameters.progress == turnIndices(iTurn,2)) = true;
end
end
