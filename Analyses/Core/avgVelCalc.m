function avgVel = avgVelCalc(MPCRecStruct)
% Calc average vel for clean runs
%   Written by Jake Olson, april 2017

% 6.666 pixels is ~2cm square bins for platform - RECOMMENDED
% probably 6 pixels would be about the same distance for the track, since
% it is about 1 brick lower. Try 6 pixels - RECOMMENDED
% For the platform, 300 by 300 pixelSize variables gives a 45 bin by 45 bin square.
% so 3 pixels = 1cm.
% for doug's old data, track looks 5/7 size, so use 2 pixels for vel.

uniqueRecs = unique([MPCRecStruct.rat;MPCRecStruct.rec]','rows');
for iRec = 1:size(uniqueRecs,1)
    currRec = find(MPCRecStruct.rat == uniqueRecs(iRec,1) & MPCRecStruct.rec == uniqueRecs(iRec,2),1);
    pathList = MPCRecStruct.pathList{currRec};
    pathRunsLineMarkers = MPCRecStruct.pathRunsLineMarkers{currRec};
    
    velThisRec = zeros(0,1);
    for iPath = 1:numel(pathList)
        thisPath = pathList(iPath);
        for iRun = 1:numel(pathRunsLineMarkers{iPath})
            velThisRec = [velThisRec;...
                MPCRecStruct.velPixPerSecs{currRec}(pathRunsLineMarkers{iPath,1}(iRun):...
                pathRunsLineMarkers{iPath,2}(iRun),1)];
        end
    end
    if MPCRecStruct.rat(currRec) <= 14 % Jake's rats - UCSD
        avgVel(iRec) = nanmean(velThisRec)./3; % so now in cm/s
    else % Doug's rats - NSI
        avgVel(iRec) = nanmean(velThisRec)./2; % so now in cm/s
    end
end
