%% Instructions for MPC Paper - Written starting in January 2017. This data
% is a collection of MPC data from Doug's NSI days, an animal done by Doug
% here (UCSD), and my (Jake) animals at UCSD. It has also been processed multiple
% different times, with Doug and his lab assistants doing initial data
% processing, then me starting it again in 2014, and finally again in 2017.
% I include this information to help explain the sometimes overly
% complicated process of compiling the data.

% Previously, all of the data from Doug's NSI animals (60,66,68,73,79) as
% well as NS12 (recorded at UCSD by Doug/assistants) and PK3 (recorded at
% UCSD by me) had been processed by him and then me. In January 2017,
% wanting to add data from NS13 and NS14 and in an effort to assure all
% data is processed similarly for the paper, I rescored all of the old data
% with the 2017 data processing tools, accessing the raw data through the
% rmap .mat files for all of the data except NS13 and NS14 (It is saved
% there).

% The process for each rmap .mat file (1 per recording):

%%TTTrackingPreprocessorMPC2017(varargin - either none or 1, the old rate map file name);
% 	- Takes and saves the name of the old ratemap file, the raw tracking
% 	data, the name of the old tracking file, the sampling rate of the
% 	tracking (previously calculated), the max gap allowed to be corrected
% 	for missing tracking, the locations that were fixed, as well as the
% 	tracking with one light after being fixed. Then smooths tracking and
% 	adds velocity, trajectory, angular velocity, and acceleration
% 	calculations. Saves all in the struct indRecStruct and saves to file.
% 	For NS13 and NS14 data, there are no old ratemap files, so these things
% 	are calculated in the file.

% indRecStruct fields list at this stage.
%     'dvtPathName'             tracking file name
%     'dvtFileName'             tracking file path
%     'oldRMapFile'             old rmap file name - does not exist for NS13 and NS14 data
%     'rawDVT'                  original raw tracking data
%     'trackingSampleRateHz'	sampling rate of the tracking data
%     'posMaxGapFilledSecs'     maximum window of missing data to interpolate over
%     'samplesLost'             locations that had missing tracking (for each light if 2)
%     'samplesFilled'           locations where missing tracking was filled in (for each light if 2)
%     'samplesUnfilled'         location where missing tracking was not filled in (for each light if 2)
%     'posFixed'                three column array, with each row: recording time, xCoord, yCoord. For NS13 and
%                               NS14, each row for this variable is: line number, recording time, xCoordLight1,
%                               yCoordLight1, xCoordAvgBothNeeded, yCoordAvgBothNeeded, xCoordAvgOrExisting,
%                               yCoordAvgOrExisting.
%     'posSmoothingWindowSecs'	window of time used to smooth tracking data
%     'processedDVT'            tracking data after smoothing (either the light or the avg light if there were 2)
%     'velSmoothWinSecs'		time window over which position change is recorded to calculate velocity
%     'velPixPerSecs'           velocity with columns speed, trajectory (direction)
%     'velInstPixPerSecs'		same but calculated over 1 sample instead of using the window size above
%     'accPixPerSecsSq'         acceleration caculated as change in velocity values listed above. magnitude and	direction
%     'accInstPixPerSecsSq'     same but calculated from velInst values
%     'angVelRadPerSecs'		change in trajectory angle calculated from velocity above
%     'angVelInstRadPerSecs'	same but calculated from velInst
 
%% TTAutoPosTrackerMPC2017;
% 	- Guides you through opening the ProcessedDVT file, marking the
% 	beginning and ending of the open field portion (if exists) then the
% 	maze running session, then marking the beginning and ending of the runs
% 	1-10 in order: LRL,LRR,RLL,RLR,LLR,LLL,RRR,RRL, LL(return end of 1 to
% 	start),RR (return end of 4 to start). Text guides click order, build
% 	gates at each spot. Autoscores the runs marking beginning and ends of
% 	runs. Marks runs that do not maintain a velocity above the
% 	minSpeedAllowed constant value as dirty runs, the rest as clean.
% 
% This adds to the indRecStruct the following fields:
%     'sessionTimeStamps'		line in processedDVT of start and end of sessions labeled in the program.
%     'events'                  three column variable with errors (will fix in TTPosTrackerMPC2017) and event code:
%                               number of run is start, number plus 100 is end, both numbers multiplied by -1 if
%                               dirty run, positive if clean.
%     'minSpeedAllowed'         constant that velocity must exceed for entire run for run to be labeled as clean.

%% TTPosTrackerMPC2017;
% 	- Human now checks the veracity of the runs determined in the AutoPosTracker, going through both clean and dirty runs and assuring correct labeling. Save before exiting, will not autosave.
% 
% This adds to the indRecStruct the following fields:
%     'pixelDVT'        the same thing as 'processedDVT' with an added column to label the color it should
%        				be plotted as (based off of what run the sample is in, if any).
%     'events'			fixes the first two columns of events, so the columns are now line number, time, and
%        				event code.
 
%% load TTBinTemplate.mat
%% TTLinearRatemapperMPC2017(TTBinTemplate);
% 	- This program provides an interface so that clicks can be made to mark
% 	the linear paths. It then maps tracking of clean runs to the closest
% 	bins to form the linear rate map. Smoothing is done while the map is
% 	created.
% 
% This adds to the indRecStruct the following fields:
%     'sessionTimeStamps'		existed already for all but NS14, grabbed from SUB paper rmaps
%     'events'			existed already for all but NS14, grabbed from SUB paper rmaps
%     'minSpeedAllowed'		existed already for all but NS14, grabbed from SUB paper rmaps
%     'pixelDVT'			existed already for all but NS14, grabbed from SUB paper rmaps
% 
%     'tfileList'			list of spike time files, pulled from old rmaps for old and NS14 data. Grabbed from 				raw data files for NS13
%     'allTfiles'			the spike times of all cells from the tfileList in a cell array. same origins as 				tfileList
%     'nCells'			simple count of number of tfiles.
%     'binTemplateMatrix'		the matrix detailing the correct number of bins for each segment of the linear 				templates for approximately 1 cm bins.
%     'filterWidthLinearRMaps'	max number of bins affected by filter (smoothing)
%     'filterSigmaLinearRMaps'	std dev of filter
%     'filterLinearRMaps'		normalized filter values (sums to 1)
%     'pathList'			list of paths that have clean runs for the recording
%     'pathRunsLineMarkers'	lines of the start/end times for runs in the pos/DVT file
%     'pathRunsTimeMarkers'	timestamp of the start/end points for runs in the pos/DVT file
%     'nRunsEachPath'		count of clean runs for each path
%     'masterTemplate'		the locations of the clicks made in the user interface that scaffold the linear rate 				templates
%     'Clicks'			the locations of the clicks in order for each path
%     'PathTemplate'		the locations of all bins in order for each path
%     'meanTemplateOcc'		the mean locations of the samples attributed to each bin for each path
%     'templateOccupancyStats'	stats about the occupancy in each bin for each path
%     'LinearRates'		linear rate maps for each path, each neuron, each traversal
%     'MeanLinearRates'		mean linear rate maps for each path, each neuron

%Note: Slight tweaking of code may be necessary to work properly for data
%from NS12/PK3 vs NSI rats. The pos files had a different number of lines
%and so I was dealing with that. I may have just changed/commented out a
%line or two instead of putting in some sort of IF case.


%% At this point, all recordings are scored and compiled, and the dataset is
% built by compiling the correct neurons from all recordings. Two structs
% are created, one of recording specific data, and one of neuron specific
% data. Obviously there are indices in both to be able to reference each
% other.
% [ MPCRecStruct, MPCNeuronStruct ] = mpcDataStructBuilder( allRMapsList, MPCRecListStruct );
%	- instructions for how to create input variables is in the file itself.

% Add two fields to MPCNeuronStruct, posNSpikesAll and posPathAndBinAll.
% Like the processedDVT, both are 1 row per tracking sample. In the spikes
% is a spike count each sample, and path and bin labels the path and bin of
% the sample if it was part of a clean run. Both variables are often useful
% to have compiled for later analyses.
% [ MPCRecStruct, MPCNeuronStruct ] = mapSpikesAndLocsToSamples( MPCRecStruct, MPCNeuronStruct);

% Add variables that mark turn directions and locations for all paths.
% MPCRecStruct = addTrackInfo( MPCRecStruct );

% Add variable marking the reward paradigm for the recording. 14 = 4 paths, 18 = 8, HL = hi lo
% MPCRecStruct = addRewardInfo( MPCRecStruct );

% Add firing rate stats (currently only track mean firing rate)
% [ MPCRecStruct, MPCNeuronStruct ] = addFRStats(MPCRecStruct, MPCNeuronStruct);

%% Total of 331 neurons (73L - 5animals, 258R - 7animals) across 9 animals.
% MPCRecStructUnanalyzed.mat and MCPNeuronStructUnanalyzed.mat in the paper
% folder are here in the pipeline.

%% Histo clean dataset
% PK3(0L 13R),NS12(0L 15R) - not included, histology did not turn out.
% Create logical vectors for neurons and recs to be analyzed.
% Throwing out animals/drives/wires that missed or we do not have histo to
% verify.
% paperRecsHistoVerified = MPCRecStruct.rat ~= 3 & MPCRecStruct.rat ~= 12;
% paperNeuronsHistoVerified =  MPCNeuronStruct.rat ~= 3 & MPCNeuronStruct.rat ~= 12;
% recsFromNotIncludedAnimals = cellfun(@(x) strcmp(x(1:3),'r12'),unique(MPCRecStruct.sourceFile)) |...
%     cellfun(@(x) strcmp(x(1:3),'r03'),unique(MPCRecStruct.sourceFile));
% 
% % clean rec struct.
% nOldRecs = size(MPCRecStruct.rat,2);
% recFields = fieldnames(MPCRecStruct);
% for iField = 1:length(recFields)
% if size(MPCRecStruct.(recFields{iField}),1) == nOldRecs
% MPCRecStruct_Final.(recFields{iField}) = MPCRecStruct.(recFields{iField})(paperRecsHistoVerified,:);
% elseif size(MPCRecStruct.(recFields{iField}),2) == nOldRecs
% MPCRecStruct_Final.(recFields{iField}) = MPCRecStruct.(recFields{iField})(:,paperRecsHistoVerified);
% else
% MPCRecStruct_Final.(recFields{iField}) = MPCRecStruct.(recFields{iField});
% end
% end
% 
% 
% % clean neuron struct
% nOldNeus = size(MPCNeuronStruct.rat,2);
% neuFields = fieldnames(MPCNeuronStruct);
% for iField = 1:length(neuFields)
% if size(MPCNeuronStruct.(neuFields{iField}),1) == nOldNeus
% MPCNeuronStruct_Final.(neuFields{iField}) = MPCNeuronStruct.(neuFields{iField})(paperNeuronsHistoVerified,:);
% elseif size(MPCNeuronStruct.(neuFields{iField}),2) == nOldNeus
% MPCNeuronStruct_Final.(neuFields{iField}) = MPCNeuronStruct.(neuFields{iField})(:,paperNeuronsHistoVerified);
% else
% % Only Mean linear rates, need to go inside.
% % Also need to add some placeholder 0s before I do the operation.
% cellSize = size(MPCNeuronStruct.(neuFields{iField}){9});
% MPCNeuronStruct.(neuFields{iField}){9} = cat(2,MPCNeuronStruct.(neuFields{iField}){9},zeros(cellSize(1),nOldNeus-cellSize(2),cellSize(3)));
% MPCNeuronStruct_Final.(neuFields{iField}) = cellfun(@(x) x(:,paperNeuronsHistoVerified,:),MPCNeuronStruct.(neuFields{iField}),'UniformOutput',false);
% end
% end
% for iNeu = 1:size(MPCNeuronStruct_Final.rat,2)
% MPCNeuronStruct_Final.recCount(iNeu) = find(MPCNeuronStruct_Final.rat(iNeu) == MPCRecStruct_Final.rat & ...
% MPCNeuronStruct_Final.rec(iNeu) == MPCRecStruct_Final.rec & ...
% arrayfun(@(x) ~isempty(x{:}),strfind(MPCRecStruct_Final.hemisphere,MPCNeuronStruct_Final.hemisphere(iNeu))),1);
% end
%

%% Total of 303 neurons (73L - 5 animals, 230R - 5 animals) across 7
% animals.
% NS13(2L 19R),NS14(23L 12R),60(8L 0R),66(32L 0R),68(0L 90R),73(0L 84R),79(8L 25R)
% save both structs in data folder at this point.
% save MPCRecStruct_Final MPCRecStruct_Final
% save MPCNeuronStruct_Final MPCNeuronStruct_Final

%% --------------- Begin analyses. --------------- %%
% MPCRecStruct_Final.mat and MCPNeuronStruct_Final.mat in the paper
% folder are here in the pipeline.
projectFolder = 'D:\NitzLab\NitzLabProjects\MPCPaperProject\'; % 'B:\MPCPaperProject\'; % 'D:\NitzLab\NitzLabProjects\MPCPaperProject\';
load([projectFolder,'Data\MPCNeuronStruct_Final.mat']);
load([projectFolder,'Data\MPCRecStruct_Final.mat']);

%% Get percent of runs that are clean for each recording. - in text
% Run in the folder you want to save in.
% Need to add ratemap folder onto path
[ percentCleanRuns ] = cleanRunPercent( MPCRecStruct_Final );
avgPercentCleanRunsAllAnimals = mean(percentCleanRuns);
% save percentCleanRunsData avgPercentCleanRunsAllAnimals percentCleanRuns 

%% Calc average vel during runs. in text
% Run in the folder you want to save in.
avgVel = avgVelCalc(MPCRecStruct_Final);
avgVelAllAnimals = mean(avgVel);
% save avgVelData avgVelAllAnimals avgVel

%% Prep for Figures - discrimination tests.
% either:
% 1) if you have already run paperResultsScript, load and process the CP analyses.
loadCPAnalyses;

% or 
% 2) run CPAnalyses
% CPAnalyses;
% then run the load script, because it also does some processing.
% loadCPAnalyses;

%% GLM
% either:
% 1) run GLM single predictor models and model comparisons
% GLMforMPCpaper;

% or 
% 2) load all glm results
load([projectFolder,'Data\AnalysisResults_CurrBioRev\GLMOnePredictorModels.mat']);
load([projectFolder,'Data\AnalysisResults_CurrBioRev\GLMfullModels.mat']);

%% Supplemental Figure 2 - Firing Rate Distributions
SupFig1B_Pop_FR_Dists

%% Figure 1 - Behavior & Actions
Figure1Script;
% Figure 1 last 2 panels - Behavior Control
Figure1_CPAnalysesBehControl;

%% Figure 2 - Action Sequence - Includes Supplemental Figure 5 
Figure2_SpaceScript;
Figure2_TimeScript;

%% Figure 34 - Context - spatial (3), choice (4)
Figures34Script;
% Example neuron plots
Figures3EFGScript;
Figures4EFGScript;

%% GLM Figure
SupFig4_GLMFigure;

%% Supplemental - Example Neurons 90% and 2/3
% Depends on GLMFigure results
SupFig2_ExtraExamples;

%% Supplemental - Example Neurons - Conjunctive
SupFig3_ExtraExamplesConjunctive;

















